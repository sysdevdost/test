"use strict";
// Class definition

var user_datatable = function() {
	// Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.table-user').KTDatatable({
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'No',
					width: '5px',
					autoHide: false,
				},
				{
					field: 'Name',
					autoHide: false,
				},
				{
					field: 'AccessLevel',
					autoHide: false,
				},
				{
					field: 'Office',
					autoHide: false,
				},
				{
					field: 'Username',
					autoHide: false,
				},
				{
					field: 'Email',
					autoHide: false,
				},
				{
					field: 'Action',
					autoHide: false,
				}
			]

		});
	};

	return {
		// Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	$('body').tooltip({selector: '[data-toggle="tooltip"]'});
	user_datatable.init();
});