"use strict";
// Class definition

var office_datatable = function() {
	// Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.table-office').KTDatatable({
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'No',
					width: '5%',
					autoHide: false,
				},

				{
					field: 'Code',
					autoHide: false,
				},
				{
					field: 'Description',
					autoHide: false,
				},
				{
					field: 'Action',
					autoHide: false,
				}
			]

		});
	};

	return {
		// Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	office_datatable.init();
});