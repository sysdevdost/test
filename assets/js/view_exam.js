"use strict";
var KTDatatablesDataSourceHtml = function() {

	var initTable1 = function() {
		var table = $('#kt_table-exam');

		// begin first table
		table.DataTable({
			responsive: true,
			columnDefs: [
				{
					targets: -1,
					title: '',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item" href="javascript:;" id="row-settings">Settings</li>
                                <li class="dropdown-item" href="javascript:;" id="row-print-preview">Print Preview</li>
                                <li class="dropdown-item" href="javascript:;" id="row-candidates">Duplicate</li>
                                <li class="dropdown-item" href="javascript:;" id="row-results">Delete</li>
                            </div>
                        </span>`;
					},
				}
			],
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
	
	$('#kt_table-exam tbody').on('click', 'a.detail-open', function () {
		var dataset = $(this).data('question');
		var set_type = $(this).data('set_type');
		var arrdataset = '';
		$.each(dataset, function(i, item) {
			arrdataset = arrdataset + 
						'<tr class="addtrows">'+
							'<td style="padding-left: 30px;"> <i class="flaticon-more"></i> &nbsp;'+(i+1)+'</td>'+
							'<td style="padding-left: 20px;">'+item.question_item+'</td>'+
							'<td>'+set_type+'</td>'+
							'<td>ANSWER</td>'+
							'<td>STATUS</td>'+
							'<td>'+
								'<span class="dropdown">'+
									'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
										'<i class="flaticon-more-v2"></i>'+
									'</a>'+
									'<div class="dropdown-menu dropdown-menu-right">'+
										'<a class="dropdown-item" href="javascript:;" id="sub-settings">Edit</a>'+
										'<a class="dropdown-item" href="javascript:;" id="sub-duplicate">Duplicate</a>'+
										'<a class="dropdown-item" href="javascript:;" id="sub-delete">Delete</a>'+
									'</div>'+
								'</span>'+
							'</td>'+
						'</tr>';
		});
		console.log(dataset);
		$(this).find('i').removeClass('fa-caret-right');
		$(this).find('i').addClass('fa-caret-down');
        $(this).removeClass('detail-open');
        $(arrdataset).insertAfter($(this).closest('tr'));
        $(this).addClass('detail-close');
    });

    $('#kt_table-exam tbody').on('click', 'a.detail-close', function () {
    	$(this).find('i').removeClass('fa-caret-down');
    	$(this).find('i').addClass('fa-caret-right');
        $(this).removeClass('detail-close');
        $(this).closest('tr').nextUntil('tr.exam-row').remove();
        $(this).addClass('detail-open');
    });

    /* BEGIN WYSIWYG */
    var KTSummernoteDemo = function () {    
        var demos = function () {
            $('.summernote').summernote({
                height: 150
            });
        }
        return {
            init: function() {
                demos(); 
            }
        };
    }();
    // Initialization
    jQuery(document).ready(function() {
        KTSummernoteDemo.init();
    });
    /* END WYSIWYG */

    $('#dpsub a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });

    $('#dpsub a.test_categ').on("click", function(e){
        $('#txtcateg').val($(this).data('id'));
    });

    $('#dpsub a.test_type').on("click", function(e){
    	var quest_type = $(this).data('id');
        $('#txtquest_type').val($(this).data('id'));
        /* 1: Enumerate */
        /* 2: Essay */
        if(quest_type == 2){
        	$('#modal_essay').modal('show');
        }
        /* 3: Fill in the blanks */
        if(quest_type == 3){
        	$('#modal_fill_in_the_blanks').modal('show');
        }
        /* 4: Matching Type */
        if(quest_type == 4){
        	$('#modal_matching_type').modal('show');
        }
        /* 5: Mathematical Equation */
        if(quest_type == 5){
        	$('#modal_essay').modal('show');
        }
        /* 6: Multiple Choice */
        if(quest_type == 6){
        	$('#modal_multiple_choice').modal('show');
        }
        /* 7: Sequencing */
        if(quest_type == 7){
        	$('#modal_fill_in_the_blanks').modal('show');
        }
        /* 8: True of False */
        if(quest_type == 8){
        	$('#modal_true_or_false').modal('show');
        }
    });
    
    $('#modal_multiple_choice').on('shown.bs.modal', function () {
	    $('#selnum_format').select2({
	        placeholder: 'Select Format',allowClear: true
	    });
	});
    $('#btnPreview-multiple-choice').on('click',function() {
    	$('#modal_question_print_preview').modal('show');
    });

    $('#btnPreview-true_false').on('click',function() {
    	$('#modal_question_print_preview').modal('show');
    });

    $('#modal_fill_in_the_blanks').on('shown.bs.modal', function () {
	    $('#selnum_format1').select2({
	        placeholder: 'Select Format',allowClear: true
	    });
	});

	$('#btnPreview-essay').on('click',function() {
    	$('#modal_question_print_preview').modal('show');
    });

	$('div#row-answers').on('click','li#row-add-below',function() {
	    $('div#row-answers').append('<div class="form-group row kt-margin-t-10">'+
	    								'<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>'+
	    								'<div class="col-lg-9 col-md-9 col-sm-12">'+
	    									'<div class="inline-small-input">'+
												'<label class="kt-checkbox"><input type="checkbox" class="chkmc-correct-answer"><span></span></label>'+
												'<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">'+
												'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
								  					'<i class="la la-ellipsis-h"></i>'+
												'</a>'+
												'<div class="dropdown-menu dropdown-menu-right">'+
												    '<li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>'+
												    '<li class="dropdown-divider"></li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-add-below">Add Below</li>'+
												'</div>'+
											'</div>'+	
	    								'</div>'+
	    						    '</div>');
	}); 

	$('div#row-answers').on('click','li#row-add-below-fitb',function() {
	    $('div#row-answers').append('<div class="form-group row kt-margin-t-10">'+
	    								'<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>'+
	    								'<div class="col-lg-9 col-md-9 col-sm-12">'+
	    									'<div class="inline-small-input">'+
												'<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 10%;margin-top: -4px;">'+
												'<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">'+
												'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
								  					'<i class="la la-ellipsis-h"></i>'+
												'</a>'+
												'<div class="dropdown-menu dropdown-menu-right">'+
												    '<li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>'+
												    '<li class="dropdown-divider"></li>'+
												    '<li class="dropdown-item" href="javascript:;" id="row-add-below-fitb">Add Below</li>'+
												'</div>'+
											'</div>'+	
	    								'</div>'+
	    						    '</div>');
	}); 

	$('div#row-answers').on('click','li#row-delete',function() {
		$(this).closest('.form-group').remove();
	}); 

	$('div#row-answers').on('click','li#row-move-up',function() {
		var current_e = $(this).closest('.form-group');
		current_e.prev().insertAfter(current_e);			
	}); 

	$('div#row-answers').on('click','li#row-move-down',function() {
		var current_e = $(this).closest('.form-group');
		current_e.next().insertBefore(current_e);			
	}); 

	$('div#row-answers').on('click','li#row-import-image',function() {
		$('#modal_import_image').modal('show');
	}); 


});