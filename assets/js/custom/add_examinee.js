jQuery(document).ready(function() {
	
	$('#error-examinee').hide();
	$('.kt-selectpicker').selectpicker();
	
	$('#selexaminee').select2({
	    placeholder: ""
	});

	$('#seloffice').on('keyup keypress change',function() {
		validate_select('#seloffice','Office must not be empty.');
	});

	$('#selexaminee').on('keyup keypress change',function() {
		var examinee = $('#selexaminee').val();
		if(examinee=='NULL'){
			$('#div-select2').css("border", "solid");
			$('#div-select2').css("border-width", "1px");
			$('#div-select2').css("border-color", "#fd27eb");
			$('#div-select2').css("border-radius", "4px");
			$('#error-examinee').css("color", "#fd27eb");
			$('#error-examinee').show();
		}else{
			$('#div-select2').css("border-color", "#1dc9b7");
			$('#error-examinee').hide();
		}
	});

	$('#submit-user').click(function(e) {
		var total_error = 0;

		total_error = total_error + validate_select('#seloffice','Office must not be empty.');

		var examinee = $('#selexaminee').val();
		$('#error-examinee').hide();
		if(examinee=='NULL'){
			$('#div-select2').css("border", "solid");
			$('#div-select2').css("border-width", "1px");
			$('#div-select2').css("border-color", "#fd27eb");
			$('#div-select2').css("border-radius", "4px");
			$('#error-examinee').css("color", "#fd27eb");
			$('#error-examinee').show();
			total_error = total_error + 1;
		}else{
			$('#div-select2').css("border-color", "#1dc9b7");
		}

		if(total_error > 0){
		    e.preventDefault();
		}
	});

	$('.btnremove-examinee').on('click', function(e){
	    alert();
	});

});