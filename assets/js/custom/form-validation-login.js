function validate_select(el,msg='')
{
    $(el).closest('.dropdown.bootstrap-select.form-control.kt-').next('div.invalid-feedback').remove();
    if($(el).val() == null){
        $(el).val('');
    }
    if($(el).val() != '' && $(el).val().replace(/\s/g, '').length > 0){
        $(el).removeClass('is-invalid');
        $(el).closest('.dropdown.bootstrap-select.form-control.kt-').removeClass('is-invalid');
        $(el).addClass('is-valid');
        $(el).closest('.dropdown.bootstrap-select.form-control.kt-').addClass('is-valid');
        return 0;
    }else{
        $(el).closest('.dropdown.bootstrap-select.form-control.kt-').removeClass('is-valid');
        $(el).removeClass('is-valid');
        $(el).closest('.dropdown.bootstrap-select.form-control.kt-').addClass('is-invalid');
        $(el).addClass('is-invalid');
        $('<div class="invalid-feedback">'+msg+'</div>').insertAfter($(el).closest('.dropdown.bootstrap-select.form-control.kt-'));
        return 1;
    }
}

function check_null(el,msg)
{
    $(el).next('div.invalid-feedback').remove();
    if($(el).val() == null){
        $(el).val('');
    }
    if($(el).val() != '' && $(el).val().replace(/\s/g, '').length > 0){
        $(el).removeClass('is-invalid');
        $(el).addClass('is-valid');
        return 0;
    }else{
        $(el).removeClass('is-valid');
        $(el).addClass('is-invalid');
        $('<div class="invalid-feedback">'+msg+'</div>').insertAfter($(el));
        return 1;
    }
}

function check_valid_email(el)
{
    $(el).next('div.invalid-feedback').remove();
    if($(el).val() == null){
        $(el).val('');
    }
    if($(el).val() != '' && $(el).val().replace(/\s/g, '').length > 0){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})$/;
        if(regex.test($(el).val())){
            $(el).removeClass('is-invalid');
            $(el).addClass('is-valid');
            return 0;
        }else{
            $(el).removeClass('is-valid');
            $(el).addClass('is-invalid');
            $('<div class="invalid-feedback">Invalid Email.</div>').insertAfter($(el));
            return 1;
        }
    }else{
        $(el).removeClass('is-valid');
        $(el).addClass('is-invalid');
        $('<div class="invalid-feedback">Email must not be empty.</div>').insertAfter($(el));
        return 1;
    }
}

function check_integer(el,msg='')
{
    $(el).next('div.invalid-feedback').remove();
    if($(el).val() == null){
        $(el).val('');
    }
    if($(el).val() != '' && $(el).val().replace(/\s/g, '').length > 0){
        var el_val = $(el).val();
        if(Math.floor(el_val) == el_val && $.isNumeric(el_val)){
            $(el).removeClass('is-invalid');
            $(el).addClass('is-valid');
            return 0;
        }else{
            $(el).removeClass('is-valid');
            $(el).addClass('is-invalid');
            $('<div class="invalid-feedback">Invalid input.</div>').insertAfter($(el));
            return 1;
        }
    }else{
        $(el).removeClass('is-valid');
        $(el).addClass('is-invalid');
        $('<div class="invalid-feedback">'+msg+'</div>').insertAfter($(el));
        return 1;
    }
}

function check_num_len(el,numlen,msg)
{
    var el_val = $(el).val();
    el_val = el_val.replace('+63','');
    el_val = el_val.replace('_','');
    el_val = el_val.replace('(','');
    el_val = el_val.replace(')','');
    el_val = el_val.replace(' ','');
    el_val = el_val.replace('-','');

    $(el).next('div.invalid-feedback').remove();
    if(el_val == null){
        el_val = '';
    }
    if(el_val != '' && el_val.replace(/\s/g, '').length > 0){
        var num_match = el_val.match(/\d+/);
        if(num_match[0].length == numlen){
            $(el).removeClass('is-invalid');
            $(el).addClass('is-valid');
            return 0;
        }else{
            $(el).removeClass('is-valid');
            $(el).addClass('is-invalid');
            $('<div class="invalid-feedback">Invalid input.</div>').insertAfter($(el));
            return 1;
        }
    }else{
        $(el).removeClass('is-valid');
        $(el).addClass('is-invalid');
        $('<div class="invalid-feedback">'+msg+'</div>').insertAfter($(el));
        return 1;
    }
}

function check_pword_match(pw,cpw)
{
    var error = 0;
    $(pw).next('div.invalid-feedback').remove();
    $(cpw).next('div.invalid-feedback').remove();

    if($(pw).val() == null){
        $(pw).val('');
    }
    if($(cpw).val() == null){
        $(cpw).val('');
    }

    if(($(pw).val() != '' && $(pw).val().replace(/\s/g, '').length > 0) && ($(cpw).val() != '' && $(cpw).val().replace(/\s/g, '').length > 0)){
        $(pw).removeClass('is-invalid');
        $(pw).addClass('is-valid');

        $(cpw).removeClass('is-invalid');
        $(cpw).addClass('is-valid');
    }else{
        $(pw).removeClass('is-valid');
        $(pw).addClass('is-invalid');
        $('<div class="invalid-feedback">Password and Confirm Password must not be blank.</div>').insertAfter($(pw));
        
        $(cpw).removeClass('is-valid');
        $(cpw).addClass('is-invalid');
        $('<div class="invalid-feedback">Password and Confirm Password must not be blank.</div>').insertAfter($(cpw));
        error = error + 1;
    }

    if(error < 1){
        if($(cpw).val() != $(pw).val()){
            $(cpw).removeClass('is-valid');
            $(cpw).addClass('is-invalid');
            $('<div class="invalid-feedback">Password not match.</div>').insertAfter($(cpw));

            $(pw).removeClass('is-valid');
            $(pw).addClass('is-invalid');
            $('<div class="invalid-feedback">Password not match.</div>').insertAfter($(pw));
            error = error + 1;
        }
    }

    return error;
}
