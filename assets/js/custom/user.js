jQuery(document).ready(function() {
	$('.add-office').hide();
	$('.kt-selectpicker').selectpicker();
	
	$('#selhrmis, #selhrmis_validate').select2({
	    placeholder: "Select HRMIS Account"
	});

	$("#txtmobileno").inputmask("mask", {
	    "mask": "+63(999) 999-9999"
	});

	$('#seloffice').on('keyup keypress change',function() {
		if($(this).val()=='others'){
			$('.add-office').show();
		}else{
			$('.add-office').hide();
		}
		// validate_select('#seloffice','Office must not be empty.');
	});

	//email address
	$("#txtemail").inputmask({
	    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
	    greedy: false,
	    onBeforePaste: function (pastedValue, opts) {
	        pastedValue = pastedValue.toLowerCase();
	        return pastedValue.replace("mailto:", "");
	    },
	    definitions: {
	        '*': {
	            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
	            cardinality: 1,
	            casing: "lower"
	        }
	    }
	});

	$('#txtfirstname').on('keyup keypress change',function() {
		check_null('#txtfirstname','Firstname must not be empty.');
	});

	$('#seloffice').on('keyup keypress change',function() {
		validate_select('#seloffice','Office must not be empty.');
	});

	$('#txtlastname').on('keyup keypress change',function() {
		check_null('#txtlastname','Lastname must not be empty.');
	});

	$('#txtage').on('keyup keypress change',function() {
		check_null('#txtage','Age must not be empty.');
	});

	$('#txtemail').on('keyup keypress change',function() {
		check_valid_email('#txtemail','Email must not be empty.');
	});

	$('#txtmobileno').on('keyup keypress change',function() {
		check_num_len('#txtmobileno',10,'Mobile number must not be empty.');
	});

	$('#txtuname').on('keyup keypress change',function() {
		check_null('#txtuname','Username must not be empty.');
	});

	$('#txtpass').on('keyup keypress change',function() {
		check_null('#txtpass','Password must not be empty.');
	});

	$('#txtoffice_code').on('keyup keypress change',function() {
		check_null('#txtoffice_code','Office must not be empty.');
	});

	$('#txtoffice_desc').on('keyup keypress change',function() {
		check_null('#txtoffice_desc','Office must not be empty.');
	});

	$("input[name='gender']").change(function() {
		$('.div-gender').find('div.invalid-feedback').remove();
		$('.label-gender').css('color','#676c9a');
	});

	$('#submit-user').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtfirstname','Firstname must not be empty.');
		total_error = total_error + validate_select('#seloffice','Office must not be empty.');
		total_error = total_error + check_null('#txtlastname','Lastname must not be empty.');
		total_error = total_error + check_null('#txtage','Age must not be empty.');
		total_error = total_error + check_valid_email('#txtemail','Email must not be empty.');
		total_error = total_error + check_num_len('#txtmobileno',10,'Mobile number must not be empty.');
		total_error = total_error + check_null('#txtuname','Username must not be empty.');

		if($('#txtaction').val() == 'add'){
			total_error = total_error + check_null('#txtpass','Password must not be empty.');
		}

		$('.div-gender').find('div.invalid-feedback').remove();
		$('.label-gender').css('color','#676c9a');
		if($('#chkgenderf').is(':checked') == false && $('#chkgenderm').is(':checked') == false){
			$('.label-gender').css('color','#fd28eb');
			total_error = total_error + 1;
		}

		if($('#seloffice').val()=='others'){
			total_error = total_error + check_null('#txtoffice_code','Office must not be empty.');
			total_error = total_error + check_null('#txtoffice_desc','Office must not be empty.');
			// $('.div-gender').append('<div class="invalid-feedback">Password and Confirm Password must not be blank.</div>');
		}

		if(total_error > 0){
		    e.preventDefault();
		}
	});

	$('#selhrmis').on('keyup keypress change',function() {
		$('.add-office').hide();
		$hrmis_user = $(this).val();

		$.ajax({
			url : $('#txtbaseurl').val()+'libraries/user/api_user?empid='+$hrmis_user,
			type : "GET",
			dataType: 'json',
			success : function(data){
				// console.log(data);
				$('#txtfirstname').val(data.firstname).trigger('change');
				$('#txtmidname').val(data.middlename).trigger('change');
				$('#txtlastname').val(data.surname).trigger('change');
				$('#txtextname').val(data.nameExtension).trigger('change');
				// Age
				if(data.sex.toLowerCase() == 'f'){
					$("#chkgenderf").prop("checked", true);
				}else{
					$("#chkgenderm").prop("checked", true);
				}
				// Birthday
				if(data.birthday!=''){
					bday = data.birthday.split('-');
					age = parseInt($('#yearnow').val()) - parseInt(bday[0]);
					$('#txtage').val(age).trigger('change');
				}
				
				$('#txtemail').val(data.email).trigger('change');
				$('#txtmobileno').val(data.mobile).trigger('change');
				$('#txtuname').val(data.userName).trigger('change');

				// office
				$('#txtoffice_code').val(data.office_code).trigger('change');
				$('#txtoffice_desc').val(data.office_name).trigger('change');
				$.ajax({
					url : $('#txtbaseurl').val()+'libraries/office/json_office?code='+data.office_code,
					type : "GET",
					dataType: 'json',
					success : function(office_data){
						if(office_data!=''){
							$("#seloffice").val(office_data.office_id).trigger('change');
							$('.add-office').hide();
						}else{
							$("#seloffice").val(0).trigger('change');
							$('.add-office').show();
						}
						
					}
				});
			}
		});

	});

});

