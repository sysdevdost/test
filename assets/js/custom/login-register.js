$(document).ready(function() {
	$('.divoffice').hide();

	$('#kt_login_signup').click(function() {
		$('.kt-login__signin,.kt-login__account').hide();
		$('.kt-login__signup').show();
	});
	if($('#txtpost').val()=='1'){
		$('.kt-login__signin,.kt-login__account').hide();
		$('.kt-login__signup').show();
	}
	
	$('#kt_login_forgot').click(function() {
		$('.kt-login__signin,.kt-login__account').hide();
		$('.kt-login__forgot').show();
	});

	// sign in
	$('#txtuname').on('keyup keypress change',function() {
		check_null('#txtuname','Username must not be empty.');
	});

	$('#txtpass').on('keyup keypress change',function() {
		check_null('#txtpass','Password must not be empty.');
	});

	$('#btnsign_in').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtuname','Username must not be empty.');
		total_error = total_error + check_null('#txtpass','Password must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}
	});

	// forget Password
	//email address
	$("#txtforget_email,#txtemail").inputmask({
	    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
	    greedy: false,
	    onBeforePaste: function (pastedValue, opts) {
	        pastedValue = pastedValue.toLowerCase();
	        return pastedValue.replace("mailto:", "");
	    },
	    definitions: {
	        '*': {
	            validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
	            cardinality: 1,
	            casing: "lower"
	        }
	    }
	}); 

	$('#txtforget_email').on('keyup keypress change',function() {
		check_valid_email('#txtforget_email');
	});

	$('#btnforget_password').click(function(e) {
		if(check_valid_email('#txtforget_email') > 0){
		    e.preventDefault();
		}
	});

	// register
	// phone number format
	$("#txtmobileno").inputmask("mask", {
	    "mask": "+63(999) 999-9999"
	}); 

	$('#txtfname').on('keyup keypress change',function() {
		check_null('#txtfname','Firstname must not be empty.');
	});

	$('#txtmname').on('keyup keypress change',function() {
		check_null('#txtmname','Middlename must not be empty.');
	});

	$('#txtlname').on('keyup keypress change',function() {
		check_null('#txtlname','Lastname must not be empty.');
	});

	$('#seloffice').on('keyup keypress change',function() {
		if($(this).val()=='others'){
			$('.divoffice').show();
		}else{
			$('.divoffice').hide();
		}
		validate_select('#seloffice','Office must not be empty.');
	});

	$('#txtoffice_code').on('keyup keypress change',function() {
		check_null('#txtoffice_code','Office must not be empty.');
	});
	$('#txtoffice_desc').on('keyup keypress change',function() {
		check_null('#txtoffice_desc','Office must not be empty.');
	});

	$('#txtage').on('keyup keypress change',function() {
		check_integer('#txtage','Age must not be empty.');
	});

	$('#txtemail').on('keyup keypress change',function() {
		check_valid_email('#txtemail');
	});

	$('#txtmobileno').on('keyup keypress change',function() {
		check_num_len('#txtmobileno',10,'Mobile number must not be empty.');
	});

	$('#txtusername').on('keyup keypress change',function() {
		check_null('#txtusername','Username must not be empty.');
	});

	$('#txtpassword').on('keyup keypress change',function() {
		check_pword_match('#txtpassword','#txtrpassword');
	});

	$('#txtrpassword').on('keyup keypress change',function() {
		check_pword_match('#txtpassword','#txtrpassword');
	});

	$("input[name='gender']").change(function() {
		$('.div-gender').find('div.invalid-feedback').remove();
		$('.label-gender').css('color','#676c9a');
	});

	$('#btnregister').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtfname','Firstname must not be empty.');
		total_error = total_error + check_null('#txtmname','Middlename must not be empty.');
		total_error = total_error + check_null('#txtlname','Lastname must not be empty.');
		total_error = total_error + check_integer('#txtage','Age must not be empty.');
		total_error = total_error + check_valid_email('#txtemail');
		total_error = total_error + check_num_len('#txtmobileno',10,'Mobile number must not be empty.');
		total_error = total_error + check_null('#txtusername','Username must not be empty.');
		total_error = total_error + check_pword_match('#txtpassword','#txtrpassword');
		total_error = total_error + validate_select('#seloffice','Office must not be empty.');

		$('.div-gender').find('div.invalid-feedback').remove();
		$('.label-gender').css('color','#676c9a');
		if($('#chkgenderf').is(':checked') == false && $('#chkgenderm').is(':checked') == false){
			$('.label-gender').css('color','#fd28eb');
			total_error = total_error + 1;
			$('.div-gender').append('<div class="invalid-feedback">Password and Confirm Password must not be blank.</div>');
		}

		if($('#seloffice').val()=='others'){
			total_error = total_error + check_null('#txtoffice_code','Office must not be empty.');
			total_error = total_error + check_null('#txtoffice_desc','Office must not be empty.');
		}

		if(total_error > 0){
		    e.preventDefault();
		}
	});

	// hrmis sign in
	$('#txthrmis-uname').on('keyup keypress change',function() {
		check_null('#txthrmis-uname','Username must not be empty.');
	});

	$('#txthrmis-pass').on('keyup keypress change',function() {
		check_null('#txthrmis-pass','Password must not be empty.');
	});

	$('#btnhrmis_login').click(function(e) {
		$('span#spn_invalid_user').html('');
		var total_error = 0;

		total_error = total_error + check_null('#txthrmis-uname','Username must not be empty.');
		total_error = total_error + check_null('#txthrmis-pass','Password must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}else{
			$.ajax({
				url : $('#txtbaseurl').val()+'libraries/user/hrmis_login?fingerprint=!7D$0@9&uname='+$('#txthrmis-uname').val()+'&pass='+$('#txthrmis-pass').val(),
				type : "GET",
				dataType: 'json',
				success : function(data){
					if(data.length == 0){
						$('#txthrmis-uname,#txthrmis-pass').removeClass('is-valid');
						$('#txthrmis-uname,#txthrmis-pass').addClass('is-invalid');
						$('span#spn_invalid_user').html('invalid user');
					}else{
						$('#txtfname').val(data.firstname).trigger('change');
						$('#txtmname').val(data.middlename).trigger('change');
						$('#txtlname').val(data.surname).trigger('change');
						$('#txtextname').val(data.nameExtension).trigger('change');
						$('#txtemail').val(data.email).trigger('change');
						$('#txtmobileno').val(data.mobile).trigger('change');
						$('#txtusername').val(data.userName).trigger('change');
						// Age
						if(data.sex.toLowerCase() == 'f'){
							$("#chkgenderf").prop("checked", true);
						}else{
							$("#chkgenderm").prop("checked", true);
						}
						// Birthday
						if(data.birthday!=''){
							bday = data.birthday.split('-');
							age = parseInt($('#yearnow').val()) - parseInt(bday[0]);
							$('#txtage').val(age).trigger('change');
						}
						// office
						$('#txtoffice_code').val(data.office_code).trigger('change');
						$('#txtoffice_desc').val(data.office_name).trigger('change');
						$.ajax({
							url : $('#txtbaseurl').val()+'libraries/office/json_office?code='+data.office_code,
							type : "GET",
							dataType: 'json',
							success : function(office_data){
								if(office_data!=''){
									$("#seloffice").val(office_data.office_id).trigger('change');
								}else{
									$("#seloffice").val(0).trigger('change');
								}
							}
						});
						$('#modal-login-hrmis').modal('hide');
					}
				}
			});

		}
	});

});

