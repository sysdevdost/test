jQuery(document).ready(function() {
	$('.kt-selectpicker').selectpicker();

	$('#seloffice').on('keyup keypress change',function() {
		validate_select('#seloffice','Office must not be empty.');
	});

	$('#txttitle').on('keyup keypress change',function() {
		check_null('#txttitle','Title must not be empty.');
	});

	$('#txtdesc').on('keyup keypress change',function() {
		check_null('#txtdesc','Description must not be empty.');
	});

	$('#txtpassper').on('keyup keypress change',function() {
		check_null('#txtpassper','Invalid input.');
	});

	$('#submit-exam').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txttitle','Title must not be empty.');
		total_error = total_error + validate_select('#seloffice','Office must not be empty.');
		total_error = total_error + check_null('#txtdesc','Description must not be empty.');
		total_error = total_error + check_null('#txtpassper','Passing percentage must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}
	});

});