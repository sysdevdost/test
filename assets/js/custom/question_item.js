$(document).ready(function() {

	$('#txtitem').on('keyup keypress change',function() {
		check_null('#txtitem','Item must not be empty.');
	});

	$('#txtans_min').on('keyup keypress change',function() {
		check_null('#txtans_min','Invalid input.');
	});

	$('#txtans_max').on('keyup keypress change',function() {
		check_null('#txtans_max','Invalid input.');
	});

	$('#txtans_pts').on('keyup keypress change',function() {
		check_null('#txtans_pts','Invalid input.');
	});

	$('#submit-question-item').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtitem','Item must not be empty.');
		total_error = total_error + check_null('#txtans_min','Minimum answer must not be empty.');
		total_error = total_error + check_null('#txtans_max','Maximum answer must not be empty.');
		total_error = total_error + check_null('#txtans_pts','Point/s must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}
	});

});