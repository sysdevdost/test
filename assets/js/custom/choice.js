$(document).ready(function() {

	$('#txtdesc').on('keyup keypress change',function() {
		check_null('#txtdesc','Choice Description must not be empty.');
	});

	$('#submit-choice').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtdesc','Choice Description must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}
	});

});