"use strict";
// Class definition

var examinee_table = function() {
	// Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.table-examinee').KTDatatable({
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'No',
					width: '5%',
					autoHide: false,
				},

				{
					field: 'Name',
					autoHide: false,
				},
				{
					field: 'Email',
					autoHide: false,
				},
				{
					field: 'Action',
					autoHide: false,
				}
			]

		});
	};

	return {
		// Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	examinee_table.init();
	$('.kt-selectpicker').selectpicker();

	$('.table-examinee').on('click', 'a#btnremove-examiner', function(e){
		$('#txtexaminer').val($(this).data('id'));
		$('#modal-remove-examiner').modal('show');
	});
		
});