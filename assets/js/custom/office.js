$(document).ready(function() {
	
	$('#txtcode').on('keyup keypress change',function() {
		check_null('#txtcode','Code must not be empty.');
	});

	$('#txtdesc').on('keyup keypress change',function() {
		check_null('#txtdesc','Description must not be empty.');
	});

	$('#submit-office').click(function(e) {
		var total_error = 0;

		total_error = total_error + check_null('#txtcode','Code must not be empty.');
		total_error = total_error + check_null('#txtdesc','Description must not be empty.');

		if(total_error > 0){
		    e.preventDefault();
		}
	});

});