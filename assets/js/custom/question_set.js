$(document).ready(function() {
	$('.kt-selectpicker').selectpicker();

	$('#selquest_type').on('keyup keypress change',function() {
		if($(this).val() == 1){
			$('.div-enumeration').show();
		}else{
			$('.div-enumeration').hide();
		}
		if($(this).find(':selected').attr('data-subj') == 1){
			$('.div-minwords').show();
		}else{
			$('.div-minwords').hide();
		}
		validate_select('#selquest_type','Question type must not be empty.');
	});

	$('#txtinstruction').on('keyup keypress change',function() {
		check_null('#txtinstruction','Instruction must not be empty.');
	});

	$('#txttimelimit').on('keyup keypress change',function() {
		check_null('#txttimelimit','Invalid input.');
	});

	$('#txtminwords').on('keyup keypress change',function() {
		check_null('#txtminwords','Invalid input.');
	});

	$("input[name='radidff']").change(function() {
		$('.div-diff').find('div.invalid-feedback').remove();
		$('.label-diff').css('color','#676c9a');
	});

	$('#submit-question-set').click(function(e) {
		var total_error = 0;

		total_error = total_error + validate_select('#selquest_type','Question type must not be empty.');
		total_error = total_error + check_null('#txtinstruction','Instruction must not be empty.');
		total_error = total_error + check_null('#txttimelimit','Time limit must not be empty.');
		
		if($('#selquest_type').find(':selected').attr('data-subj') == 1){
			total_error = total_error + check_null('#txtminwords','Minimum words must not be empty.');
		}

		$('.div-diff').find('div.invalid-feedback').remove();
		$('.label-diff').css('color','#676c9a');
		if(!$("input[name='radidff']:checked").val()){
			$('.label-diff').css('color','#fd28eb');
			total_error = total_error + 1;
		}

		if(total_error > 0){
		    e.preventDefault();
		}
	});

});