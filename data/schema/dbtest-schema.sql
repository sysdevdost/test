-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 21, 2019 at 11:44 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.0.33-12+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbtest-schema`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblaccesslevel`
--

CREATE TABLE `tblaccesslevel` (
  `access_level_id` int(11) NOT NULL,
  `access_level_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblaccesslevel`
--

INSERT INTO `tblaccesslevel` (`access_level_id`, `access_level_desc`) VALUES
(1, 'System Administrator'),
(2, 'Examiner'),
(3, 'Examinee');

-- --------------------------------------------------------

--
-- Table structure for table `tblchangelog`
--

CREATE TABLE `tblchangelog` (
  `log_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `log_module` text NOT NULL,
  `log_sql` text NOT NULL,
  `log_desc` text NOT NULL,
  `log_json` text,
  `log_user` int(11) NOT NULL,
  `log_date` datetime NOT NULL,
  `log_ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblchoices`
--

CREATE TABLE `tblchoices` (
  `choice_id` int(11) NOT NULL,
  `qitem_id` int(11) NOT NULL,
  `choice_desc` text NOT NULL,
  `is_correct` tinyint(2) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbldifficulty`
--

CREATE TABLE `tbldifficulty` (
  `difficulty_level_id` int(11) NOT NULL,
  `difficulty_level_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldifficulty`
--

INSERT INTO `tbldifficulty` (`difficulty_level_id`, `difficulty_level_desc`) VALUES
(1, 'Easy'),
(2, 'Moderate'),
(3, 'Difficult');

-- --------------------------------------------------------

--
-- Table structure for table `tblexam`
--

CREATE TABLE `tblexam` (
  `exam_id` int(11) NOT NULL,
  `exam_title` text NOT NULL,
  `exam_desc` text NOT NULL,
  `passing_percentage` int(11) NOT NULL,
  `is_final` tinyint(2) NOT NULL,
  `is_published` tinyint(4) NOT NULL DEFAULT '0',
  `is_active` tinyint(2) NOT NULL DEFAULT '1',
  `office_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblexaminee`
--

CREATE TABLE `tblexaminee` (
  `examinee_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `finish_exam` tinyint(2) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblexamineeanswer`
--

CREATE TABLE `tblexamineeanswer` (
  `answer_id` int(11) NOT NULL,
  `qitem_id` int(11) NOT NULL,
  `choice_id` int(11) DEFAULT NULL,
  `answer_text` text NOT NULL,
  `time_spent` int(11) NOT NULL,
  `is_correct` tinyint(2) NOT NULL DEFAULT '0',
  `answer_points` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `answer_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblexaminer`
--

CREATE TABLE `tblexaminer` (
  `examiner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbloffice`
--

CREATE TABLE `tbloffice` (
  `office_id` int(11) NOT NULL,
  `office_code` varchar(20) DEFAULT NULL,
  `office_desc` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblquestionitem`
--

CREATE TABLE `tblquestionitem` (
  `qitem_id` int(11) NOT NULL,
  `ques_set_id` int(11) NOT NULL,
  `question_item` text NOT NULL,
  `min_answer` int(11) NOT NULL,
  `max_answer` int(11) NOT NULL,
  `choices_random` tinyint(2) NOT NULL DEFAULT '0',
  `answer_points` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblquestionset`
--

CREATE TABLE `tblquestionset` (
  `ques_set_id` int(11) NOT NULL,
  `ques_type_id` int(11) NOT NULL,
  `ques_set_instruction` text,
  `difficulty_level_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_random` tinyint(2) NOT NULL DEFAULT '0',
  `time_limit` int(11) NOT NULL DEFAULT '0',
  `allow_skip` tinyint(2) NOT NULL DEFAULT '1',
  `min_words` int(11) DEFAULT '0',
  `enum_no_of_item` int(11) DEFAULT NULL,
  `write_minus_wrong` tinyint(2) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_updated_by` int(11) DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblquestiontype`
--

CREATE TABLE `tblquestiontype` (
  `ques_type_id` int(11) NOT NULL,
  `ques_type_desc` varchar(50) NOT NULL,
  `is_subjective` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblquestiontype`
--

INSERT INTO `tblquestiontype` (`ques_type_id`, `ques_type_desc`, `is_subjective`) VALUES
(1, 'Enumeration', 1),
(2, 'Essay', 1),
(3, 'Fill in the blanks', 1),
(4, 'Matching Type', 0),
(5, 'Mathematical Equation', 1),
(6, 'Multiple Choice', 0),
(7, 'Sequencing', 0),
(8, 'True of False', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblresults`
--

CREATE TABLE `tblresults` (
  `result_id` int(11) DEFAULT NULL,
  `ques_set_id` int(11) NOT NULL,
  `ques_set_result` int(11) NOT NULL,
  `remarks` tinyint(2) NOT NULL DEFAULT '0',
  `generated_by` int(11) NOT NULL,
  `generated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluserprofile`
--

CREATE TABLE `tbluserprofile` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(100) NOT NULL,
  `user_middlename` varchar(100) DEFAULT NULL,
  `user_lastname` varchar(100) NOT NULL,
  `user_extname` varchar(20) DEFAULT NULL,
  `user_age` int(2) DEFAULT NULL,
  `user_gender` char(1) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `last_updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(30) DEFAULT NULL,
  `user_password` varchar(60) DEFAULT NULL,
  `hrmis_empnumber` varchar(20) NOT NULL,
  `access_level_id` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `office_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_updated_by` int(11) NOT NULL,
  `last_updated_date` datetime NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblaccesslevel`
--
ALTER TABLE `tblaccesslevel`
  ADD PRIMARY KEY (`access_level_id`);

--
-- Indexes for table `tblchangelog`
--
ALTER TABLE `tblchangelog`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `tblchoices`
--
ALTER TABLE `tblchoices`
  ADD PRIMARY KEY (`choice_id`);

--
-- Indexes for table `tbldifficulty`
--
ALTER TABLE `tbldifficulty`
  ADD PRIMARY KEY (`difficulty_level_id`);

--
-- Indexes for table `tblexam`
--
ALTER TABLE `tblexam`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `tblexaminee`
--
ALTER TABLE `tblexaminee`
  ADD PRIMARY KEY (`examinee_id`);

--
-- Indexes for table `tblexaminer`
--
ALTER TABLE `tblexaminer`
  ADD PRIMARY KEY (`examiner_id`);

--
-- Indexes for table `tbloffice`
--
ALTER TABLE `tbloffice`
  ADD PRIMARY KEY (`office_id`);

--
-- Indexes for table `tblquestionitem`
--
ALTER TABLE `tblquestionitem`
  ADD PRIMARY KEY (`qitem_id`);

--
-- Indexes for table `tblquestionset`
--
ALTER TABLE `tblquestionset`
  ADD PRIMARY KEY (`ques_set_id`);

--
-- Indexes for table `tblquestiontype`
--
ALTER TABLE `tblquestiontype`
  ADD PRIMARY KEY (`ques_type_id`);

--
-- Indexes for table `tbluserprofile`
--
ALTER TABLE `tbluserprofile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblaccesslevel`
--
ALTER TABLE `tblaccesslevel`
  MODIFY `access_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblchangelog`
--
ALTER TABLE `tblchangelog`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblchoices`
--
ALTER TABLE `tblchoices`
  MODIFY `choice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbldifficulty`
--
ALTER TABLE `tbldifficulty`
  MODIFY `difficulty_level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblexam`
--
ALTER TABLE `tblexam`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblexaminee`
--
ALTER TABLE `tblexaminee`
  MODIFY `examinee_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblexaminer`
--
ALTER TABLE `tblexaminer`
  MODIFY `examiner_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbloffice`
--
ALTER TABLE `tbloffice`
  MODIFY `office_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblquestionitem`
--
ALTER TABLE `tblquestionitem`
  MODIFY `qitem_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblquestionset`
--
ALTER TABLE `tblquestionset`
  MODIFY `ques_set_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblquestiontype`
--
ALTER TABLE `tblquestiontype`
  MODIFY `ques_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
