<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	var $arrData;

	function __construct() {
        parent::__construct();
    }

	public function index()
	{
		$this->template->load('template/template-main', 'reports/report', $this->arrData);
	}
	
	

}
