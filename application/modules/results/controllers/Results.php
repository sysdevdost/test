<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Results extends MY_Controller {

	var $arrData;

	function __construct() {
        parent::__construct();
    }

	public function index()
	{
		$this->template->load('template/template-main', 'results/result', $this->arrData);
	}
	
	

}
