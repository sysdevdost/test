<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function getdata($examid='')
	{
		$exams = $this->db->join('tbldifficulty','tbldifficulty.difficulty_level_id = tbltest.difficulty_level_id')
						->get_where('tbltest',array('is_active' => 1))->result_array();

		foreach($exams as $key=>$exam):
			$examinees = $this->get_examinees_byexam($exam['exam_id']);
			$exams[$key]['taken'] = count($examinees);
			$exams[$key]['last_taken'] = $examinees[0]['finished_date'];
			$exams[$key]['category'] = $this->get_set_byexam($exam['exam_id']);
		endforeach;

		return $exams;
	}

	public function get_examinees_byexam($examid)
	{
		return $this->db->order_by('finished_date','desc')->get_where('tblexaminee',array('exam_id' => $examid))->result_array();
	}

	public function get_set_byexam($examid)
	{
		$this->db->join('tblquestiontype','tblquestiontype.ques_type_id = tblcategories.ques_type_id');
		return $this->db->get_where('tblcategories',array('exam_id' => $examid))->result_array();
	}
	
	public function addexam($arrdata)
	{
		$this->db->insert('tbltest',$arrdata);
		return $this->db->insert_id();
	}

	public function save($arrdata,$examid)
	{
		$this->db->where('exam_id',$examid);
		$this->db->update('tbltest',$arrdata);
		return $this->db->affected_rows();
	}

	// public function deleteoffice($officeid)
	// {
	// 	$this->db->where('office_id',$officeid);
	// 	$this->db->delete('tbloffice'); 	
	// 	return $this->db->affected_rows();
	// }

	// public function check_exist($code)
	// {
	// 	$res = $this->db->get_where('tbloffice', array('office_code' => $code))->result_array();

	// 	return count($res) > 0 ? 1 : 0;
	// }

}