<div class="modal fade" id="modal_row_settings" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">Test Settings</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>ID No.</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" id="txtexam_id" name="txtexam_id" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Name/Title</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control" id="txtexam_name" name="txtexam_name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Description</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_desc" name="txtexam_desc"> </textarea>
						</div>
					</div>
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-3 col-sm-12"><i><b>COPY TEST</b></i></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<select class="form-control kt-select2" id="selcopy_test" name="selcopy_test">
								<?php foreach($exams as $exam_copy): ?>
									<option value="<?=$exam_copy['exam_id']?>"><?=$exam_copy['exam_title']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Level of Difficulty</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<select class="form-control kt-select2" id="seldifficulty" name="seldifficulty">
								<?php foreach($difficulty_level as $diff): ?>
									<option value="<?=$diff['difficulty_level_id']?>"><?=$diff['difficulty_level_desc']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkinstruction" id="chkinstruction">
							<b>Instruction</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="summernote" id="txtinstructions" name="txtinstructions"></div>
						</div>
					</div>
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row kt-margin-t-10">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Settings</b></label>
						<div class="form-group col-lg-4">
							<div class="kt-checkbox-list">
								<label class="kt-checkbox">
									<input type="checkbox" name="chkgraded" id="chkgraded"> Graded questions
									<span></span>
								</label>
								<label class="kt-checkbox">
									<input type="checkbox" name="chktimer" id="chktimer"> Apply timer
									<span></span>
								</label>
								<label class="kt-checkbox">
									<input type="checkbox" name="chknav" id="chknav"> Allow navigation during test
									<span></span>
								</label>
							</div>
						</div>
						<div class="form-group col-lg-4">
							<div class="kt-checkbox-list">
								<label class="kt-checkbox">
									<input type="checkbox" name="chkrandom_question" id="chkrandom_question"> Show questions in random order
									<span></span>
								</label>
								<label class="kt-checkbox">
									<input type="checkbox" name="chkrandom_answer" id="chkrandom_answer"> Show answers in random order
									<span></span>
								</label>
								<label class="kt-checkbox">
									<input type="checkbox" name="chkselect_candidates" id="chkselect_candidates"> Allow selected candidates only
									<span></span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row kt-margin-t-10">
						<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="inline-small-input">
								<input type="text" class="form-control form-control-sm" name="txtno_question" id="txtno_question">
								<label>No. of questions per page </label>
							</div>
							<div class="inline-small-input">
								<input type="text" class="form-control form-control-sm" name="txtpassing_mark" id="txtpassing_mark">
								<label>Passing mark (0 if none) </label>
							</div>
							<div class="inline-small-input">
								<input type="text" class="form-control form-control-sm" name="txtno_attempts" id="txtno_attempts">
								<label>No. of attempts allowed (0 if unlimited) </label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal modal-stick-to-bottom fade" id="modal_row_print_preview" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_row_candidates" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_row_results" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_row_history" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">History</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<table class="table no-border">
					<thead>
						<tr>
							<th>Date</th>
							<th>Time</th>
							<th>Event</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Date</td>
							<td>Time</td>
							<td>Event</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_row_publish" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Bootstrap Date Time Picker Examples</h5>
				<button type="reset" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-2 col-sm-12"><b>Minimum Setup</b></label>
						<label class="col-form-label col-lg-1 col-sm-12"><b>From</b></label>
						<div class="col-lg-5">
							<div class="input-group date">
								<input type="text" class="form-control" placeholder="Select date" id="txtpublish_date_from" name="txtpublish_date_from" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar glyphicon-th"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="input-group date">
								<input type="text" class="form-control" placeholder="Select time" id="txtpublish_time_from" name="txtpublish_time_from" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o glyphicon-th"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row kt-margin-t-20">
						<label class="col-form-label col-lg-2 col-sm-12"><b>&nbsp;</b></label>
						<label class="col-form-label col-lg-1 col-sm-12"><b>To</b></label>
						<div class="col-lg-5">
							<div class="input-group date">
								<input type="text" class="form-control" placeholder="Select date" id="txtpublish_date_to" name="txtpublish_date_to" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar glyphicon-th"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="input-group date">
								<input type="text" class="form-control" placeholder="Select time" id="txtpublish_time_to" name="txtpublish_time_to" />
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-clock-o glyphicon-th"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row kt-margin-b-20">
						<label class="col-form-label col-lg-2 col-sm-12"><b>Remarks</b></label>
						<div class="col-lg-10 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtpublish_remarks" name="txtpublish_remarks"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-brand" data-dismiss="modal">Close</button>
					<button type="reset" class="btn btn-secondary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_sub_settings" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Category Properties</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="modal-body">
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12"><b>ID No.</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="txtexam_id" name="txtexam_id" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Name/Title</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="txtexam_name" name="txtexam_name">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Description</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<textarea class="form-control" id="txtexam_desc" name="txtexam_desc"> </textarea>
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Numbering Format</b></label>
							<div class="form-group col-lg-4">
								<input type="text" class="form-control" name="txtnum_format" id="txtnum_format">
							</div>
							<div class="form-group col-lg-4">
								<select class="form-control kt-select2" id="selnum_format_part" name="selnum_format_part">
									<?php foreach(num_formats() as $format): ?>
										<option value="<?=$format['name']?>" data-sample_val="<?=$format['sample_val']?>"><?=$format['format']?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Position</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" id="txtexam_name" name="txtexam_name">
							</div>
						</div>
						<div class="form-group row kt-margin-t-20">
							<label class="col-form-label col-lg-3 col-sm-12">
								<input type="checkbox" name="chkinstruction" id="chkinstruction">
								<b>Instruction</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="summernote" id="txtinstructions_category" name="txtinstructions_category"></div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Time Limit</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" id="txtno_question">
									<label>Hours </label>
									<input type="text" class="form-control form-control-sm" name="txtno_question" id="txtno_question">
									<label>Minutes </label>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12"><b>Max. Questions</b></label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" id="txtno_question">
									<label><b>Numbering Format</b> </label>
									<select class="form-control kt-select2" id="selnum_format_categ" name="selnum_format_categ" style="width: 45%;">
										<?php foreach(num_formats() as $format): ?>
											<option value="<?=$format['name']?>" data-sample_val="<?=$format['sample_val']?>"><?=$format['format']?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_sub_print_preview" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_sub_duplicate" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_sub_delete" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>