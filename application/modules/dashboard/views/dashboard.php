<style type="text/css">
	.nav-link.dropdown-toggle:after, .btn.dropdown-toggle:after {
    	content: '' !important;
	}
	.kt-notification.kt-notification--fit {
	    width: 500px !important;
	}
	i.la.la-ellipsis-h {
	    font-weight: bold !important;
	    color: #2786fb !important;
	    font-size: 20px !important;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Tests
				</h3>
			</div>
			<div class="kt-portlet__head-label">
				<a href="" class="btn btn-brand"><i class="fa fa-plus"></i> New</a>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table" id="kt_table-test">
				<thead>
					<tr>
						<th>#</th>
						<th>Test Name</th>
						<th>Level of Difficulty</th>
						<th>Taken</th>
						<th>Last Taken</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($exams as $exam): ?>
						<tr class="exam-row">
							<td>
								<a href="javascript:;" class="detail-open" data-set='<?=json_encode($exam["category"])?>'>
									<i class="fa fa-caret-right"></i></a>
								<a href="<?=base_url('exam/view_exam/'.$exam['exam_id'])?>"><?=str_repeat('&nbsp;',5)?><u><?=fix_id($exam['exam_id'])?></u></a>
							</td>
							<td><?=$exam['exam_title']?></td>
							<td><?=$exam['difficulty_level_desc']?></td>
							<td><?=$exam['taken']?></td>
							<td><?=$exam['last_taken']!='' ? date('Y-m-d',strtotime($exam['last_taken'])) : ''?></td>
							<td><?=$exam['stat_id']?></td>
							<td></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>

<?php include('_modal.php'); ?>
<script>
	"use strict";
	var KTDatatablesDataSourceHtml = function() {

		var initTable1 = function() {
			var table = $('#kt_table-test');

			// begin first table
			table.DataTable({
				responsive: true,
				columnDefs: [
					{
						targets: -1,
						title: '',
						orderable: false,
						render: function(data, type, full, meta) {
							return `
	                        <span class="dropdown">
	                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
	                              <i class="la la-ellipsis-h"></i>
	                            </a>
	                            <div class="dropdown-menu dropdown-menu-right">
	                                <a class="dropdown-item" href="javascript:;" id="row-settings">Settings</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-print-preview">Print Preview</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-candidates">Candidates</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-results">Results</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-history">History</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-publish">Publish</a>
	                                <a class="dropdown-item" href="javascript:;" id="row-category"><i class="la la-plus"></i> Add Category</a>
	                            </div>
	                        </span>`;
						},
					},
					{
						targets: 5,
						render: function(data, type, full, meta) {
							var status = {
								1: {'title': 'New', 'class': ' kt-badge--info'},
								2: {'title': 'Open', 'class': ' kt-badge--success'},
								3: {'title': 'Close', 'class': ' kt-badge--danger'},
							};
							if (typeof status[data] === 'undefined') {
								return data;
							}
							return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
						},
					}
				],
			});

		};

		return {

			//main function to initiate the module
			init: function() {
				initTable1();
			},

		};

	}();

	jQuery(document).ready(function() {
		KTDatatablesDataSourceHtml.init();
		
		$('#kt_table-test tbody').on('click', 'a.detail-open', function () {
			var dataset = $(this).data('set');
			var arrdataset = '';
			$.each(dataset, function(i, item) {
				arrdataset = arrdataset + '<tr class="addtrows"> <td colspan=11>'+fix_id(item.exam_id)+' - '+item.ques_set_id+'&nbsp;'.repeat(10)+item.ques_type_desc+
	          		  	'<span class="dropdown">'+
							'<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">'+
								'<i class="la la-ellipsis-h"></i>'+
							'</a>'+
							'<div class="dropdown-menu dropdown-menu-right">'+
								'<a class="dropdown-item" href="javascript:;" id="sub-settings">Settings</a>'+
								'<a class="dropdown-item" href="javascript:;" id="sub-print-preview">Print Preview</a>'+
								'<a class="dropdown-item" href="javascript:;" id="sub-duplicate">Duplicate</a>'+
								'<a class="dropdown-item" href="javascript:;" id="sub-delete">Delete</a>'+
							'</div>'+
						'</span>'+
					   '</td>'+
				'</tr>';
			});
			$(this).find('i').removeClass('fa-caret-right');
			$(this).find('i').addClass('fa-caret-down');
	        $(this).removeClass('detail-open');
	        $(arrdataset).insertAfter($(this).closest('tr'));
	        $(this).addClass('detail-close');
	    });

	    $('#kt_table-test tbody').on('click', 'a.detail-close', function () {
	    	$(this).find('i').removeClass('fa-caret-down');
	    	$(this).find('i').addClass('fa-caret-right');
	        $(this).removeClass('detail-close');
	        $(this).closest('tr').nextUntil('tr.exam-row').remove();
	        $(this).addClass('detail-open');
	    } );

	    // row link
	    $('#kt_table-test tbody').on( 'click', '#row-settings', function (){
		   $('#modal_row_settings').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#row-print-preview', function (){
		   $('#modal_row_print_preview').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#row-candidates', function (){
		   $('#modal_row_candidates').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#row-results', function (){
		   $('#modal_row_results').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#row-history', function (){
		   $('#modal_row_history').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#row-publish', function (){
		   $('#modal_row_publish').modal('show');
		});

		// sub row link
	    $('#kt_table-test tbody').on( 'click', '#sub-settings', function (){
		   $('#modal_sub_settings').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#sub-print-preview', function (){
		   $('#modal_sub_print_preview').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#sub-duplicate', function (){
		   $('#modal_sub_duplicate').modal('show');
		});
		$('#kt_table-test tbody').on( 'click', '#sub-delete', function (){
		   $('#modal_sub_delete').modal('show');
		});


		$('#modal_row_settings').on('shown.bs.modal', function () {
		    $('#selcopy_test').select2({
		        placeholder: 'Select Test',allowClear: true
		    });
		    $('#seldifficulty').select2({
		        placeholder: 'Select Level of Difficulty',allowClear: true
		    });
		    
		});
		$('#modal_sub_settings').on('shown.bs.modal', function () {
			$('#selnum_format_part,#selnum_format_categ').select2({
		        placeholder: 'Select Format', allowClear: true
		    });
		});

		$('#selnum_format').on('change',function() {
			$('#txtnum_format').val($(this).find(':selected').attr('data-sample_val'));
		});

		/* BEGIN WYSIWYG */
		var KTSummernoteDemo = function () {    
		    // Private functions
		    var demos = function () {
		        $('.summernote').summernote({
		            height: 150
		        });
		        $('#txtpublish_date_from,#txtpublish_date_to').datetimepicker({
		            format: "yyyy-mm-dd",
		            todayHighlight: true,
		            autoclose: true,
		            startView: 2,
		            minView: 2,
		            forceParse: 0,
		            pickerPosition: 'bottom-left'
		        });
		        $('#txtpublish_time_from,#txtpublish_time_to').datetimepicker({
		            format: "hh:ii",
		            showMeridian: true,
		            todayHighlight: true,
		            autoclose: true,
		            startView: 1,
		            minView: 0,
		            maxView: 1,
		            forceParse: 0,
		            pickerPosition: 'bottom-left'
		        });
		    }

		    return {
		        // public functions
		        init: function() {
		            demos(); 
		        }
		    };
		}();

		// Initialization
		jQuery(document).ready(function() {
		    KTSummernoteDemo.init();
		});
		/* END WYSIWYG */
	});

</script>