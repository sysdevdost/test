<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Dashboard_model','exam/Question_set_model'));
    }

	public function index()
	{
		$this->arrdata['difficulty_level'] = $this->Question_set_model->getdifficulty();
		$this->arrdata['exams'] = $this->Dashboard_model->getdata();
		$this->template->load('template/template-main', 'dashboard/dashboard', $this->arrdata);
	}
	
	

}
