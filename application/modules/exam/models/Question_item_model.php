<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_item_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($qitemid='')
	{
		if($qitemid!=''):
			$res = $this->db->get_where('tblquestion',array('qitem_id' => $qitemid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->get_where('tblquestion',array('qitem_id' => $qitemid))->result_array();
		endif;
		
	}

	public function getdata_byset($setid)
	{
		return $this->db->get_where('tblquestion',array('ques_set_id' => $setid,'is_delete' => 0))->result_array();
	}

	public function add($arrdata)
	{
		$this->db->insert('tblquestion',$arrdata);
		return $this->db->insert_id();
	}

	public function save($arrdata,$qitemid)
	{
		$this->db->where('qitem_id',$qitemid);
		$this->db->update('tblquestion',$arrdata);
		return $this->db->affected_rows();
	}

}