<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Choices_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($choiceid='')
	{
		if($choiceid!=''):
			$res = $this->db->get_where('tblchoices',array('choice_id' => $choiceid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->get_where('tblchoices')->result_array();
		endif;
	}

	public function getdata_byqitem($quesid)
	{
		return $this->db->get_where('tblchoices',array('qitem_id' => $quesid,'is_delete' => 0))->result_array();
	}

	public function add($arrdata)
	{
		$this->db->insert('tblchoices',$arrdata);
		return $this->db->insert_id();
	}
	
	public function update_bychoice($arrdata,$choiceid)
	{
		$this->db->where('choice_id',$choiceid);
		$this->db->update('tblchoices',$arrdata);
		return $this->db->affected_rows();
	}

	public function update_byqitem($arrdata,$qitemid)
	{
		$this->db->where('qitem_id',$qitemid);
		$this->db->update('tblchoices',$arrdata);
		echo $this->db->last_query();
		return $this->db->affected_rows();
	}

}