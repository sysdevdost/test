<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exam_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	// public function getdata($examid='')
	// {
	// 	if($examid!=''):
	// 		$res = $this->db->join('tblexaminer','tblexaminer.exam_id = tblexam.exam_id')
	// 						->join('tbluserprofile','tbluserprofile.user_id = tblexaminer.user_id')
	// 						->join('tblusers','tblusers.user_id = tbluserprofile.user_id')
	// 						->get_where('tblexam',array('tblexam.exam_id' => $examid))->result_array();

	// 		return count($res) > 0 ? $res[0] : array();
	// 	else:
	// 		return $this->db->join('tblexaminer','tblexaminer.exam_id = tblexam.exam_id')
	// 						->join('tbluserprofile','tbluserprofile.user_id = tblexaminer.user_id')
	// 						->join('tblusers','tblusers.user_id = tbluserprofile.user_id')
	// 						->get_where('tblexam')->result_array();
	// 	endif;
	// }

	public function getdata($examid='')
	{
		if($examid!=''):
			$res = $this->db->join('tbluserprofile','tbluserprofile.user_id = tbltest.created_by')
							->join('tblusers','tblusers.user_id = tbluserprofile.user_id')
							->join('tbloffice','tbloffice.office_id = tblusers.office_id')
							->get_where('tbltest',array('exam_id' => $examid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->join('tbluserprofile','tbluserprofile.user_id = tbltest.created_by')
							->join('tblusers','tblusers.user_id = tbluserprofile.user_id')
							->join('tbloffice','tbloffice.office_id = tblusers.office_id')
							->get_where('tbltest',array('is_active' => 1))->result_array();
		endif;
	}

	public function addexam($arrdata)
	{
		$this->db->insert('tbltest',$arrdata);
		return $this->db->insert_id();
	}

	public function save($arrdata,$examid)
	{
		$this->db->where('exam_id',$examid);
		$this->db->update('tbltest',$arrdata);
		return $this->db->affected_rows();
	}

	// public function deleteoffice($officeid)
	// {
	// 	$this->db->where('office_id',$officeid);
	// 	$this->db->delete('tbloffice'); 	
	// 	return $this->db->affected_rows();
	// }

	// public function check_exist($code)
	// {
	// 	$res = $this->db->get_where('tbloffice', array('office_code' => $code))->result_array();

	// 	return count($res) > 0 ? 1 : 0;
	// }

}