<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_set_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($setid='')
	{
		if($setid!=''):
			$this->db->join('tblquestiontype','tblquestiontype.ques_type_id = tblcategories.ques_type_id','left');
			$res = $this->db->get_where('tblcategories',array('ques_set_id' => $setid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			$this->db->join('tblquestiontype','tblquestiontype.ques_type_id = tblcategories.ques_type_id','left');
			return $this->db->get_where('tblcategories')->result_array();
		endif;
	}

	public function getdata_byexam($examid='')
	{
		$this->db->join('tblquestiontype','tblquestiontype.ques_type_id = tblcategories.ques_type_id','left');
		return $this->db->get_where('tblcategories',array('exam_id' => $examid,'is_delete' => 0))->result_array();
	}

	public function getdifficulty()
	{
		return $this->db->get('tbldifficulty')->result_array();
	}

	public function getquestion_type()
	{
		return $this->db->get('tblquestiontype')->result_array();
	}

	public function addset($arrdata)
	{
		$this->db->insert('tblcategories',$arrdata);
		echo $this->db->last_query();
		// return $this->db->insert_id();
	}

	public function editset($arrdata,$setid)
	{
		$this->db->where('ques_set_id',$setid);
		$this->db->update('tblcategories',$arrdata);
		return $this->db->affected_rows();
	}

	// public function deleteoffice($officeid)
	// {
	// 	$this->db->where('office_id',$officeid);
	// 	$this->db->delete('tbloffice'); 	
	// 	return $this->db->affected_rows();
	// }

	// public function check_exist($code)
	// {
	// 	$res = $this->db->get_where('tbloffice', array('office_code' => $code))->result_array();

	// 	return count($res) > 0 ? 1 : 0;
	// }

}