<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('libraries/Office_model','Exam_model','Question_set_model','Question_item_model','choices_model','examinee/Examinee_model'));
    }

    public function settings()
	{
		$arrpost = $this->input->post();
		$this->arrdata['exam_list'] = $this->Exam_model->getdata();
		$this->arrdata['arrexam'] = $this->Exam_model->getdata($_GET['examid']);

		if(!empty($arrpost)):
			$arrdata_exam = array(
								'exam_title'		 => $arrpost['txttitle'],
								'exam_desc'			 => $arrpost['txtdesc'],
								'passing_percentage' => $arrpost['txtpassper'],
								'office_id'			 => $arrpost['seloffice'],
								'is_final'			 => isset($arrpost['chkfinal']) ? 1 : 0,
								'is_published'		 => isset($arrpost['chkpublish']) ? 1 : 0,
								'created_by'		 => $_SESSION['sess_userid'],
								'created_date'		 => date('Y-m-d H:i:s')
			);
			$examid = $this->Exam_model->addexam($arrdata_exam);
			$this->session->set_flashdata('strSuccessMsg','Exam successfully added.');
			redirect('exam/view_exam/'.$examid);	
		endif;

		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'exam/exam_settings', $this->arrdata);
	}

    #############
	public function index()
	{
		if(access_level() == 'examinee'):
		else:
			$this->arrdata['arrexams'] = $this->Exam_model->getdata();
			$this->template->load('template/template-main', 'exam/_index', $this->arrdata);
		endif;
	}

	public function add()
	{
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$arrdata_exam = array(
								'exam_title'		 => $arrpost['txttitle'],
								'exam_desc'			 => $arrpost['txtdesc'],
								'passing_percentage' => $arrpost['txtpassper'],
								'office_id'			 => $arrpost['seloffice'],
								'is_final'			 => isset($arrpost['chkfinal']) ? 1 : 0,
								'is_published'		 => isset($arrpost['chkpublish']) ? 1 : 0,
								'created_by'		 => $_SESSION['sess_userid'],
								'created_date'		 => date('Y-m-d H:i:s')
			);
			$examid = $this->Exam_model->addexam($arrdata_exam);
			$this->session->set_flashdata('strSuccessMsg','Exam successfully added.');
			redirect('exam/view_exam/'.$examid);	
		endif;

		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'exam/form', $this->arrdata);
	}

	public function edit()
	{
		$examid = $this->uri->segment(3);
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$arrdata_exam = array(
								'exam_title'		 => $arrpost['txttitle'],
								'exam_desc'			 => $arrpost['txtdesc'],
								'passing_percentage' => $arrpost['txtpassper'],
								'office_id'			 => $arrpost['seloffice'],
								'is_final'			 => isset($arrpost['chkfinal']) ? 1 : 0,
								'is_published'		 => isset($arrpost['chkpublish']) ? 1 : 0,
								'last_updated_by'	 => $_SESSION['sess_userid'],
								'last_updated_date'	 => date('Y-m-d H:i:s')
			);
			$this->Exam_model->save($arrdata_exam,$examid);
			$this->session->set_flashdata('strSuccessMsg','Exam successfully updated.');
			redirect('exam/view_exam/'.$examid);	
		endif;

		$this->arrdata['arrexam'] = $this->Exam_model->getdata($examid);
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'exam/form', $this->arrdata);
	}

	public function delete()
	{
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$this->Exam_model->save(array('is_active' => 0),$arrpost['txtexamid']);
			$this->session->set_flashdata('strSuccessMsg','Exam successfully deleted.');
			redirect('exam');
		endif;
	}

	public function publish()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(3);
		$publish = $this->uri->segment(4);
		
		$this->Exam_model->save(array('is_published' => ($publish==1?0:1)),$examid);
		$this->session->set_flashdata('strSuccessMsg','Exam successfully deleted.');
		redirect('exam');
	}

	public function view_exam()
	{
		$examid = $this->uri->segment(3);
		# get exam details
		$exam = $this->Exam_model->getdata($examid);
		# get question set details
		$exam['question_set'] = $this->Question_set_model->getdata_byexam($examid);
		# get question item
		foreach($exam['question_set'] as $key => $question_set):
			$question_item = $this->Question_item_model->getdata_byset($question_set['ques_set_id']);
			# get choices
			foreach($question_item as $qkey=>$ques):
				$question_item[$qkey]['choices'] = $this->choices_model->getdata_byqitem($ques['qitem_id']);
			endforeach;

			$exam['question_set'][$key]['question_item'] = $question_item;
		endforeach;

		$this->arrdata['exam'] = $exam;
		$this->arrdata['quest_types'] = $this->Question_set_model->getquestion_type();
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'exam/exam_view', $this->arrdata);
	}

	// public function view_exam()
	// {
	// 	$examid = $this->uri->segment(3);
	// 	# get exam details
	// 	$exam = $this->Exam_model->getdata($examid);
	// 	# get question set details
	// 	$exam['question_set'] = $this->Question_set_model->getdata_byexam($examid);
	// 	# get question item
	// 	foreach($exam['question_set'] as $key => $question_set):
	// 		$question_item = $this->Question_item_model->getdata_byset($question_set['ques_set_id']);
	// 		# get choices
	// 		foreach($question_item as $qkey=>$ques):
	// 			$question_item[$qkey]['choices'] = $this->choices_model->getdata_byqitem($ques['qitem_id']);
	// 		endforeach;

	// 		$exam['question_set'][$key]['question_item'] = $question_item;
	// 	endforeach;

	// 	$this->arrdata['exam'] = $exam;
	// 	$this->arrdata['arroffice'] = $this->Office_model->getdata();
	// 	$this->arrdata['action'] = 'add';
	// 	$this->template->load('template/template-main', 'exam/_view', $this->arrdata);
	// }

	public function take_exam()
	{
		# get exam details
		$examid = $this->uri->segment(3);
		$exam = $this->Exam_model->getdata($examid);
		$this->arrdata['exam'] = $exam;

		# get question set details
		$question_set = $this->Question_set_model->getdata_byexam($examid);
		$set_id = $this->uri->segment(4);
		$set_id = !is_numeric($set_id) ? $question_set[0]['ques_set_id'] : $set_id;
		
		$this->arrdata['question_set'] = $question_set;
		$this->arrdata['current_question_set'] = $this->Question_set_model->getdata($set_id);
		$this->arrdata['set_id'] = $set_id == '' ? $question_set[0]['ques_set_id'] : $set_id;

		$question_items = array();
		if($set_id!=''):
			$question_items = $this->Question_item_model->getdata_byset($set_id);
		endif;

		# get question item
		$question_id = $this->uri->segment(5);
		$question_id = !is_numeric($question_id) ? $question_items[0]['qitem_id'] : $question_id;

		$this->arrdata['question_items'] = $question_items;
		$this->arrdata['question_id'] = $question_id;
		
		# question index
		$question_index = $this->uri->segment(6);
		$question_index = !is_numeric($question_index) ? 0 : $question_index;
		$prev_questions = $question_items[$question_index-1];
		$next_questions = $question_items[$question_index+1];
		$this->arrdata['question_index'] = $question_index;

		if(count($question_items) == ($question_index+1)):
			$set_key = array_search($set_id, array_column($question_set, 'ques_set_id'));
			$new_set = array_slice($question_set,$set_key);
			$question_set = $new_set[1];
			$set_id = $new_set[1]['ques_set_id'];
			$question_items = $this->Question_item_model->getdata_byset($set_id);
			$next_questions = $question_items[0];
		endif;

		$last_index_prev_question = 0;
		if($question_index == 0):
			$set_key = array_search($set_id, array_column($question_set, 'ques_set_id'));
			$question_set = $question_set[$set_key-1];
			$set_id = $question_set['ques_set_id'];
			$question_items = $this->Question_item_model->getdata_byset($set_id);
			$prev_questions = end($question_items);
			$last_index_prev_question = count($question_items)-1;
		endif;
		$this->arrdata['last_index_prev_question'] = $last_index_prev_question;
		$this->arrdata['prev_questions'] = $prev_questions;
		$this->arrdata['next_questions'] = $next_questions;

		# get choices
		$this->arrdata['choices'] = $this->choices_model->getdata_byqitem($question_id);

		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['action'] = 'take exam';
		$this->template->load('template/template-main', 'exam/examinee/_take_exam', $this->arrdata);
	}
	


}
