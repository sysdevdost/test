<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_set extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Question_set_model'));
    }

	public function add()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		if(!empty($arrpost)):
			$arrquestion_set = array(
								'ques_type_id'		  => $arrpost['selquest_type'],
								'ques_set_instruction'=> $arrpost['txtinstruction'],
								'difficulty_level_id' => $arrpost['radidff'],
								'exam_id'			  => $examid,
								'question_random'	  => isset($arrpost['chkrandom']) ? 1 : 0,
								'time_limit'		  => $arrpost['txttimelimit'],
								'allow_skip'		  => isset($arrpost['chkskip']) ? 1 : 0,
								'min_words'			  => isset($arrpost['txtminwords']) ? 1 : 0,
								'write_minus_wrong'	  => isset($arrpost['chkwmwrong']) ? 1 : 0,
								'created_by'		  => $_SESSION['sess_userid'],
								'created_date'		  => date('Y-m-d H:i:s')
			);
			$this->Question_set_model->addset($arrquestion_set);
			$this->session->set_flashdata('strSuccessMsg','Question set successfully added.');
			redirect('exam/view_exam/'.$examid);	
		endif;

		$this->arrdata['arrdifficulty'] = $this->Question_set_model->getdifficulty();
		$this->arrdata['arrquest_type'] = $this->Question_set_model->getquestion_type();

		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'question_set/form', $this->arrdata);
	}

	public function edit()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		$setid = $this->uri->segment(5);
		if(!empty($arrpost)):
			$arrquestion_set = array(
								'ques_type_id'		  => $arrpost['selquest_type'],
								'ques_set_instruction'=> $arrpost['txtinstruction'],
								'difficulty_level_id' => $arrpost['radidff'],
								'question_random'	  => isset($arrpost['chkrandom']) ? 1 : 0,
								'time_limit'		  => $arrpost['txttimelimit'],
								'allow_skip'		  => isset($arrpost['chkskip']) ? 1 : 0,
								'min_words'			  => isset($arrpost['txtminwords']) ? 1 : 0,
								'write_minus_wrong'	  => isset($arrpost['chkwmwrong']) ? 1 : 0,
								'last_updated_by'	  => $_SESSION['sess_userid'],
								'last_updated_date'	  => date('Y-m-d H:i:s')
			);
			$this->Question_set_model->editset($arrquestion_set,$setid);
			$this->session->set_flashdata('strSuccessMsg','Question set successfully updated.');
			redirect('exam/view_exam/'.$examid);	
		endif;

		$this->arrdata['question_set'] = $this->Question_set_model->getdata($setid);
		$this->arrdata['arrdifficulty'] = $this->Question_set_model->getdifficulty();
		$this->arrdata['arrquest_type'] = $this->Question_set_model->getquestion_type();

		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'question_set/form', $this->arrdata);
	}

	public function delete()
	{
		$this->Question_set_model->editset(array('is_delete' => 1),$_POST['txtsetid']);
		$this->session->set_flashdata('strSuccessMsg','Question set successfully removed.');
		redirect('exam/view_exam/'.$this->uri->segment(4));
	}
	

}
