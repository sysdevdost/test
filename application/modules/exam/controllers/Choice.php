<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Choice extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Choices_model'));
    }

	public function add()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		$setid = $this->uri->segment(5);
		$itemid = $this->uri->segment(6);

		if(!empty($arrpost)):
			$arrchoices = array(
							'qitem_id'		=> $itemid,
							'choice_desc' 	=> $arrpost['txtdesc'],
							'is_correct'	=> isset($arrpost['chkcorrect']) ? 1 : 0,
							'created_by'	=> $_SESSION['sess_userid'],
							'created_date'	=> date('Y-m-d H:i:s')
			);
			$this->Choices_model->add($arrchoices);
			$this->session->set_flashdata('strSuccessMsg','Choice successfully added.');
			redirect('exam/view_exam/'.$examid.'/'.$setid);	
		endif;

		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'choices/form', $this->arrdata);
	}

	public function edit()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		$setid = $this->uri->segment(5);
		$itemid = $this->uri->segment(6);
		$choiceid = $this->uri->segment(7);

		if(!empty($arrpost)):
			$iscorrect = isset($arrpost['chkcorrect']) ? 1 : 0;
			if($iscorrect == 1):
				$this->Choices_model->update_byqitem(array('is_correct' => 0), $itemid);
			endif;
			$arrchoices = array(
							'choice_desc' 	=> $arrpost['txtdesc'],
							'is_correct'	=> $iscorrect,
							'last_updated_by'  => $_SESSION['sess_userid'],
							'last_updated_date'=> date('Y-m-d H:i:s')
			);
			$this->Choices_model->update_bychoice($arrchoices,$choiceid);
			$this->session->set_flashdata('strSuccessMsg','Choice successfully updated.');
			redirect('exam/view_exam/'.$examid.'/'.$setid);	
		endif;
		
		$this->arrdata['arrchoice'] = $this->Choices_model->getdata($choiceid);

		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'choices/form', $this->arrdata);
	}

	public function save_answer()
	{
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$this->Choices_model->update_byqitem(array('is_correct' => 0), $_GET['qitem']);
			$this->Choices_model->update_bychoice(array('is_correct' => 1), $arrpost['radchoice']);
			$this->session->set_flashdata('strSuccessMsg','Correct answer successfully updated.');
			redirect('exam/view_exam/'.$_GET['exam'].'/'.$_GET['set']);
		endif;
	}
	
	public function delete()
	{
		$this->Choices_model->update_bychoice(array('is_delete' => 1),$_POST['txtchoiceid']);
		$this->session->set_flashdata('strSuccessMsg','Choice successfully removed.');
		redirect('exam/view_exam/'.$this->uri->segment(4));
	}


}
