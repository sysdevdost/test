<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_item extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Question_item_model','Question_set_model','Choices_model'));
    }

	public function add()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		$setid = $this->uri->segment(5);
		$question_set = $this->Question_set_model->getdata($setid);
		$this->arrdata['question_set'] = $question_set;
		if(!empty($arrpost)):
			$arrquestion_item = array(
									'ques_set_id'	=> $setid,
									'question_item' => $arrpost['txtitem'],
									'min_answer'    => $arrpost['txtans_min'],
									'max_answer'	=> $arrpost['txtans_max'],
									'remarks_answer'=> $arrpost['txtremarks'],
									'choices_random'=> isset($arrpost['chkrandom']) ? 1 : 0,
									'answer_points'	=> $arrpost['txtans_pts'],
									'no_of_items'	=> $arrpost['txtno_items'],
									'created_by'	=> $_SESSION['sess_userid'],
									'created_date'	=> date('Y-m-d H:i:s')
			);
			$itemid = $this->Question_item_model->add($arrquestion_item);
			if($question_set['ques_type_id'] == 8):
				$arrchoices = array(
								'qitem_id'		=> $itemid,
								'choice_desc' 	=> 'True',
								'is_correct'	=> 1,
								'created_by'	=> $_SESSION['sess_userid'],
								'created_date'	=> date('Y-m-d H:i:s')
				);
				$this->Choices_model->add($arrchoices);
				$arrchoices = array(
								'qitem_id'		=> $itemid,
								'choice_desc' 	=> 'False',
								'is_correct'	=> 0,
								'created_by'	=> $_SESSION['sess_userid'],
								'created_date'	=> date('Y-m-d H:i:s')
				);
				$this->Choices_model->add($arrchoices);
			endif;
			$this->session->set_flashdata('strSuccessMsg','Question item successfully added.');
			redirect('exam/view_exam/'.$examid.'/'.$setid);	
		endif;

		$this->arrdata['action'] = 'add';
		if($question_set['ques_type_id'] == 7):
			$this->arrdata['sequence_type'] = array('1' => 'Number',
													'a' => 'Small Letter',
													'A' => 'Capital Letter',
													'i' => 'Small Roman Numeral',
													'i' => 'Capital Roman Numeral');
			$this->template->load('template/template-main', 'question_item/form_sequence', $this->arrdata);
		else:
			$this->template->load('template/template-main', 'question_item/form', $this->arrdata);
		endif;
	}

	public function edit()
	{
		$arrpost = $this->input->post();
		$examid = $this->uri->segment(4);
		$setid = $this->uri->segment(5);
		$qitem = $this->uri->segment(6);
		$question_set = $this->Question_set_model->getdata($setid);
		$this->arrdata['question_set'] = $question_set;
		if(!empty($arrpost)):
			$arrquestion_item = array(
									'question_item' => $arrpost['txtitem'],
									'min_answer'    => $arrpost['txtans_min'],
									'max_answer'	=> $arrpost['txtans_max'],
									'remarks_answer'=> $arrpost['txtremarks'],
									'choices_random'=> isset($arrpost['chkrandom']) ? 1 : 0,
									'answer_points'	=> $arrpost['txtans_pts'],
									'no_of_items'	=> $arrpost['txtno_items'],
									'last_updated_by'  => $_SESSION['sess_userid'],
									'last_updated_date'=> date('Y-m-d H:i:s')
			);

			$this->Question_item_model->save($arrquestion_item,$qitem);
			$this->session->set_flashdata('strSuccessMsg','Question item successfully updated.');
			redirect('exam/view_exam/'.$examid.'/'.$setid);	
		endif;

		$this->arrdata['question_item'] = $this->Question_item_model->getdata($qitem);
		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'question_item/form', $this->arrdata);
	}
	
	public function delete()
	{
		$this->Question_item_model->save(array('is_delete' => 1),$_POST['txtqitem']);
		$this->session->set_flashdata('strSuccessMsg','Question item successfully removed.');
		redirect('exam/view_exam/'.$this->uri->segment(4));
	}

}
