<?=load_plugin('css',array('select'))?>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-file-alt"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> Question Set
				</h3>
			</div>
		</div>
		
		<div class="kt-portlet__body">
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Question Type</label>
						<div class="col-6">
							<select class="form-control kt-selectpicker" name="selquest_type" id="selquest_type" <?=$action=='delete'?'disabled':''?>>
								<option value="">-- SELECT OFFICE --</option>
								<?php foreach($arrquest_type as $type):
										$selected = isset($question_set) ? $question_set['ques_type_id'] == $type['ques_type_id'] ? 'selected' : '' : '';?>
									<option value="<?=$type['ques_type_id']?>" <?=$selected?> data-subj="<?=$type['is_subjective']?>" >
												<?=strtoupper($type['ques_type_desc'])?>
													<i><?=$type['is_subjective']?' - Subjective':''?></i>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Instruction</label>
						<div class="col-6">
							<textarea class="form-control" id="txtinstruction" name="txtinstruction"><?=isset($question_set) ? $question_set['ques_set_instruction'] : set_value('txtinstruction')?></textarea>
						</div>
					</div>
					<div class="form-group row div-diff">
						<label for="example-text-input" class="col-2 col-form-label">Level of Difficulty</label>
						<div class="col-6">
							<div class="kt-radio-inline">
								<?php foreach($arrdifficulty as $diff): ?>
									<label class="kt-radio label-diff">
										<input type="radio" name="radidff" value="<?=$diff['difficulty_level_id']?>"
												<?=isset($question_set) ? $question_set['difficulty_level_id'] == $diff['difficulty_level_id'] ? 'checked' : '' : ''?>> <?=$diff['difficulty_level_desc']?>
										<span></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Time Limit</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txttimelimit" name="txttimelimit" value="<?=isset($question_set) ? $question_set['time_limit'] : set_value('txttimelimit')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
						<div class="col-2" style="padding-top: 12px;"><i>In Minutes</i></div>
					</div>
					<div class="form-group row div-minwords" style="<?=isset($question_set) ? ($question_set['is_subjective'] == 1 ? '' : 'display: none;') : 'display:none;'?>">
						<label for="example-text-input" class="col-2 col-form-label">Min words</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txtminwords" name="txtminwords" value="<?=isset($question_set) ? $question_set['min_words'] : set_value('txtminwords')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label"></label>
						<div class="col-6">
							<label class="kt-checkbox" style="margin-right: 30px;">
								<input type="checkbox" name="chkrandom" id="chkrandom"
										<?=isset($question_set) ? $question_set['question_random'] ? 'checked' : '' : ''?>> Random
								<span></span>
							</label>
							<label class="kt-checkbox" style="margin-right: 30px;">
								<input type="checkbox" name="chkskip" id="chkskip"
										<?=isset($question_set) ? $question_set['allow_skip'] ? 'checked' : '' : ''?>> Allow Skip
								<span></span>
							</label>
							<label class="kt-checkbox" style="margin-right: 30px;">
								<input type="checkbox" name="chkwmwrong" id="chkwmwrong"
										<?=isset($question_set) ? $question_set['write_minus_wrong'] ? 'checked' : '' : ''?>> Write Minus Wrong
								<span></span>
							</label>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-question-set">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i>
									<?=$action == 'add' ? 'Submit' : ($action == 'delete' ? 'Delete' : 'Save')?>
								</button>
								<a href="<?=base_url('exam/view_exam/'.$this->uri->segment(4))?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('select','form_validation'))?>
<script src="<?=base_url('assets/js/custom/question_set.js')?>"></script>