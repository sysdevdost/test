<?=$question_no = 1; load_plugin('css',array('wizard'));?>
<br>
<pre><?php #print_r($exam) ?></pre>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet__body">
		<div class="kt-portlet ">
			<!-- begin::Exam Details -->
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=ucwords($exam['exam_title'])?>
						<small><?=ucfirst($exam['exam_desc'])?></small>		
					</h3>
				</div>
			</div>
			<!-- end::Exam Details -->

			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--tabs kt-portlet--sortable">
				<div class="kt-portlet__head">
					<!-- begin::Question Set Nav -->
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-primary" role="tablist">
							<?php foreach($question_set as $set): ?>
								<li class="nav-item">
									<a class="nav-link <?=$set_id==$set['ques_set_id']||$set_id==''?'active':''?>" href="<?=base_url('exam/take_exam/').$exam['exam_id'].'/'.$set['ques_set_id']?>">
										<i class="la la-cog"></i> <?=$set['ques_type_desc']?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<!-- end::Question Set Nav -->
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<?php foreach($question_set as $key=>$set): ?>
							<div class="tab-pane <?=$set_id==$set['ques_set_id']||$set_id==''?'active':''?>" id="set<?=$set['ques_set_id']?>" role="tabpanel">
								<?=$set['ques_set_instruction']?>
								<!-- BEGIN TEST -->
								<div class="kt-portlet__body kt-portlet__body--fit">
									<div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
										<!-- begin:: Question item number -->
										<div class="kt-grid__item kt-wizard-v2__aside">
											<div class="kt-wizard-v2__nav">
												<div class="kt-wizard-v2__nav-items">
													<?php foreach($question_items as $qctr=>$qitem): if($question_id==$qitem['qitem_id']): $question_no = $qctr+1; else: 1; endif; ?>
													<a class="kt-wizard-v2__nav-item" href="<?=base_url('exam/take_exam/').$exam['exam_id'].'/'.$set['ques_set_id'].'/'.$qitem['qitem_id'].'/'.$qctr?>" data-ktwizard-type="step" <?=$question_id==$qitem['qitem_id']||$question_id==''?'data-ktwizard-state="current"':''?>>
														<div class="kt-wizard-v2__nav-body">
															<div class="kt-wizard-v2__nav-label">
																<div class="kt-wizard-v2__nav-label-desc">
																	<b><?=$qctr+1?></b> <small><?=elipsis($qitem['question_item'],20)?></small>
																</div>
															</div>
														</div>
													</a>
													<?php endforeach; ?>
												</div>
											</div>
										</div>
										<!-- end:: Question item number -->

										<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">

											<!--begin: Form Wizard Form-->
											<form class="kt-form" id="kt_form">

												<!--begin: Form Wizard Step-->
												<div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
													<div class="kt-heading kt-heading--md">Question <?=$question_no?></div>
													<div class="kt-form__section kt-form__section--first">
														<div class="kt-wizard-v2__form">
															<div class="form-group">
																<label><?=$question_items[$question_index]['question_item']?></label>
															</div>
															<div class="form-group">
																<!-- begin essay -->
																<div <?=in_array($current_question_set['ques_type_id'],array(1,5))?'':'hidden'?>>
																	<textarea class="form-control" id="txtessay_ans"></textarea>
																</div>
																<!-- end essay -->
																<!-- begin enumeration -->
																<div <?=$current_question_set['ques_type_id']==1?'':'hidden'?>>
																	<?php foreach(range(1,$question_items[$question_index]['no_of_items']) as $enum): ?>
																		<div class="row">
																			<div class="col-md-1" style="text-align: end;padding-top: 7px;padding-right: 0px;"><?=$enum?>.</div>
																			<div class="col-md-11"><input type="text" class="form-control" name="txtessay_ans[]"></div>
																		</div>
																	<?php endforeach; ?>
																</div>
																<!-- end enumeration -->
																<!-- begin fill in the blank -->
																<div <?=$current_question_set['ques_type_id']==3?'':'hidden'?>>
																	<?php foreach(range(1,$question_items[$question_index]['max_answer']) as $enum): ?>
																		<div class="row">
																			<div class="col-md-1" style="text-align: end;padding-top: 7px;padding-right: 0px;"><?=$enum?>.</div>
																			<div class="col-md-11"><input type="text" class="form-control" name="txtessay_ans[]"></div>
																		</div>
																	<?php endforeach; ?>
																</div>
																<!-- end fill in the blank -->
																<?php foreach($choices as $choice): ?>
																	<?php if($question_items[$question_index]['max_answer'] > 1):?>
																		<div class="kt-checkbox-list">
																			<label class="kt-checkbox">
																				<input type="checkbox" name="chkchoice[]" value="<?=$choice['choice_id']?>" <?=$choice['is_correct']?'checked':''?>> <?=$choice['choice_desc']?>
																				<span></span>
																			</label>
																		</div>
																	<?php else: ?>
																		<div class="kt-radio-list">
																			<label class="kt-radio">
																				<input type="radio" name="radchoice" value="<?=$choice['choice_id']?>" <?=$choice['is_correct']?'checked':''?>> <?=$choice['choice_desc']?>
																				<span></span>
																			</label>
																		</div>
																	<?php endif; ?>
																<?php endforeach; ?>
															</div>
														</div>
													</div>
												</div>
												<!--end: Form Wizard Step-->

												<!--begin: Form Actions -->
												<div class="kt-form__actions">
													<?php $prev_questions_index = $question_index == 0 ? $last_index_prev_question : $question_index-1?>
													<?php if($last_index_prev_question >= 0): ?>
														<a class="btn btn-secondary" href="<?=base_url('exam/take_exam/').$exam['exam_id'].'/'.$prev_questions['ques_set_id'].'/'.$prev_questions['qitem_id'].'/'.$prev_questions_index?>">
															Previous
														</a>
													<?php else: echo '<div></div>'; endif; ?>
													<?php $next_questions_index = count($question_items) == $question_index+1 ? 0 : $question_index+1?>
													<a class="btn btn-brand" style="color:white;" href="<?=base_url('exam/take_exam/').$exam['exam_id'].'/'.$next_questions['ques_set_id'].'/'.$next_questions['qitem_id'].'/'.$next_questions_index?>">
														<?=count($next_questions) > 0 ? 'Next' : 'Finish' ?>	
													</a>
												</div>

												<!--end: Form Actions -->
											</form>

											<!--end: Form Wizard Form-->
										</div>
									</div>
								</div>
								<!-- END TEST -->
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>