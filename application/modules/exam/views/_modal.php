<style type="text/css">
	.form-group.row {
	    margin-bottom: 2px !important;
	}
	textarea {
	    margin-bottom: 5px;
	}
</style>
<div class="modal modal-stick-to-bottom fade" id="modal_multiple_choice" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">Question Properties
					<br><small>MULTIPLE CHOICE</small></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Position</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkinstruction" id="chkinstruction">
							<b>Question</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="summernote" id="txtmc-question" name="txtmcquestion"></div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Multiple Answer</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12" style="padding-top: 10px;">
							<label class="kt-checkbox">
								<input type="checkbox" name="chkrmultiple_answer" id="chkmc-multiple_answer">
								<span></span>
							</label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Point per answer</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Correct Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Incorrect Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Metadata</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_name" name="txtexam_name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>ANSWERS:</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12" style="margin-top: 8px;"><small>(Check correct answer)</small></div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Numbering Format</b></label>
						<div class="col-lg-4 col-md-9 col-sm-12">
							<select class="form-control kt-select2" id="selnum_format" name="selnum_format">
								<?php foreach(num_formats() as $format): ?>
									<option value="<?=$format['name']?>"><?=$format['format']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div id="row-answers">
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 1 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<label class="kt-checkbox">
										<input type="checkbox" class="chkmc-correct-answer"><span></span>
									</label>
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 2 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<label class="kt-checkbox">
										<input type="checkbox" class="chkmc-correct-answer"><span></span>
									</label>
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 3 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<label class="kt-checkbox">
										<input type="checkbox" class="chkmc-correct-answer"><span></span>
									</label>
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 4 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<label class="kt-checkbox">
										<input type="checkbox" class="chkmc-correct-answer"><span></span>
									</label>
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below">Add Below</li>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="btnPreview-multiple-choice">Preview</button>
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_true_or_false" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">Question Properties
					<br><small>TRUE OR FALSE</small></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Position</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkinstruction" id="chkinstruction">
							<b>Question</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="summernote" id="txtmc-question" name="txtmcquestion"></div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Point per answer</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Correct Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Incorrect Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Metadata</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_name" name="txtexam_name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>ANSWERS:</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12" style="margin-top: 8px;"><small>(Check correct answer)</small></div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="kt-radio-inline">
								<label class="kt-radio">
									<input type="radio" name="radio2"> True
									<span></span>
								</label>
								<label class="kt-radio">
									<input type="radio" name="radio2"> False
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="btnPreview-true_false">Preview</button>
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_fill_in_the_blanks" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">Question Properties
					<br><small>FILL-IN-THE-BLANKS [or SEQUENCING]</small></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Position</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkinstruction" id="chkinstruction">
							<b>Question</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="summernote" id="txtmc-question" name="txtmcquestion"></div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Point per answer</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Correct Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Incorrect Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Metadata</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_name" name="txtexam_name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>ANSWERS:</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12" style="margin-top: 8px;"></div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Numbering Format</b></label>
						<div class="col-lg-4 col-md-9 col-sm-12">
							<select class="form-control kt-select2" id="selnum_format1" name="selnum_format1">
								<?php foreach(num_formats() as $format): ?>
									<option value="<?=$format['name']?>"><?=$format['format']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div id="row-answers">
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 1 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 10%;margin-top: -4px;">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below-fitb">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 2 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 10%;margin-top: -4px;">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below-fitb">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 3 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 10%;margin-top: -4px;">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below-fitb">Add Below</li>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row kt-margin-t-10">
							<label class="col-form-label col-lg-3 col-sm-12">&nbsp;</label>
							<!-- option 4 -->
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="inline-small-input">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 10%;margin-top: -4px;">
									<input type="text" class="form-control form-control-sm" name="txtno_question" style="width: 100%;margin-top: -4px;margin-left: 10px;">
									<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
									  <i class="la la-ellipsis-h"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
									    <li class="dropdown-item" href="javascript:;" id="row-import-image">Import Image</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-up">Move Up</li>
									    <li class="dropdown-item" href="javascript:;" id="row-move-down">Move Down</li>
									    <li class="dropdown-item" href="javascript:;" id="row-delete">Delete</li>
									    <li class="dropdown-divider"></li>
									    <li class="dropdown-item" href="javascript:;" id="row-add-below-fitb">Add Below</li>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="btnPreview-multiple-choice">Preview</button>
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_essay" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">Question Properties
					<br><small>ESSAY [or COMPUTATIONAL]</small></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="la la-remove"></span>
				</button>
			</div>
			<form class="kt-form kt-form--fit kt-form--label-right">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Position</b></label>
						<div class="col-lg-2 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkinstruction" id="chkinstruction">
							<b>Question</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="summernote" id="txtmc-question" name="txtmcquestion"></div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Min. no. of words</b></label>
						<div class="col-lg-2">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
						<label class="col-form-label col-lg-2"><b>Point value</b></label>
						<div class="col-lg-2">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
						<label class="col-form-label col-lg-1">out of</label>
						<div class="col-lg-2">
							<input type="text" class="form-control form-control-sm" id="txtexam_id" name="txtexam_id">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Correct Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12">
							<input type="checkbox" name="chkfeedback" id="chkfeedback">
							<b>Incorrect Feedback</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<textarea class="form-control" id="txtexam_feedback" name="txtexam_feedback"> </textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-3 col-sm-12"><b>Metadata</b></label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input type="text" class="form-control form-control-sm" id="txtexam_name" name="txtexam_name">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="btnPreview-essay">Preview</button>
					<button type="button" class="btn btn-brand">Save</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal modal-stick-to-bottom fade" id="modal_question_print_preview" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">[TYPE OF EXAM]</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<div class="col-lg-12" style="display: flex;">
					<div class="col-lg-9" id="mc-p-question" style="text-align: justify;">
						1.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras et cursus leo. Fusce nunc,       dignissim eu nunc vitae, maximus aliquet lorem. Pellentesque sed viverra ante, a dictum leo. Nunc vel metus sed orci sodales mollis eu quis ipsum. 
					</div>
					<div class="col-lg-3">
						<div style="border: 1px solid #ccc;text-align: center;background-color: #ccc;color: #000;"><b>Points</b></div>
						<div id="points" style="border: 1px solid #ccc;padding: 2px;text-align: center;color: #000;font-weight: 300;">10</div>
					</div>
				</div>
				<div id="mc-div-choices"  style="margin-left: 22px;margin-top: 15px;">
					<div class="form-group">
						<div class="kt-checkbox-list">
							<label class="kt-checkbox">
								<input type="checkbox"> Default
								<span></span>
							</label>
						</div>

						<div class="form-group">
							<div class="kt-radio-list">
								<label class="kt-radio">
									<input type="radio" name="radio1"> Default
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-stick-to-bottom fade" id="modal_import_image" role="dialog" data-backdrop="false">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Import Image</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<center>
					<a class='btn btn-instagram' href='javascript:;'>
					    <i class="fa fa-upload"></i> Attach File
					    <input type="file" name ="userfile[]" id= "userfile" multiple 
					        style='left: 16px !important;width: 90%;height: 100%;position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
					        name="file_source" size="40">
					</a>
				</center>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-brand">Upload</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

