<?=form_open('exam/choice/save_answer?exam='.$exam['exam_id'].'&set='.$set['ques_set_id'].'&qitem='.$ques_item['qitem_id'])?>
<?php foreach($ques_item['choices'] as $choice): ?>
	<?php if($ques_item['max_answer'] > 1):?>
		<div class="kt-checkbox-list">
			<label class="kt-checkbox">
				<input type="checkbox" name="chkchoice[]" value="<?=$choice['choice_id']?>" <?=$choice['is_correct']?'checked':''?>> <?=$choice['choice_desc']?>
				<span></span>
			</label>
		</div>
	<?php else: ?>
		<div class="kt-radio-list">
			<label class="kt-radio">
				<input type="radio" name="radchoice" value="<?=$choice['choice_id']?>" <?=$choice['is_correct']?'checked':''?>> <?=$choice['choice_desc']?>
				<span></span>
				&nbsp;
				<a href="<?=base_url('exam/choice/edit/'.$exam['exam_id'].'/'.$set['ques_set_id'].'/'.$ques_item['qitem_id'].'/'.$choice['choice_id'])?>" style="color:#22b9ff;"><i class="fa fa-pencil-alt"></i></a>
				&nbsp;
				<a href="javascript:;" class="btndelete-choice" data-id="<?=$choice['choice_id']?>" style="color:red;"><i class="fa fa-trash"></i></a>
			</label>
		</div>
	<?php endif; ?>
<?php endforeach; ?>

<br>
<?php if(count($ques_item['choices']) > 0): ?>
	<button type="submit" class="btn btn-brand btn-sm">
		<i class="la la-check"></i> Save Answer
	</button>
<?php endif; ?>

<a href="<?=base_url('exam/choice/add/'.$exam['exam_id'].'/'.$set['ques_set_id'].'/'.$ques_item['qitem_id'])?>" class="btn btn-success btn-sm">
	<i class="la la-plus"></i> Add Choice
</a>
<?=form_close()?>