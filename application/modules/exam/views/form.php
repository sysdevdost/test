<?=load_plugin('css',array('select'))?>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon-file-1"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> Exam
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin::Form-->
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Title</label>
						<div class="col-6">
							<textarea class="form-control" name="txttitle" id="txttitle"><?=isset($arrexam) ? $arrexam['exam_title'] : ''?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Description</label>
						<div class="col-6">
							<textarea class="form-control" name="txtdesc" id="txtdesc"><?=isset($arrexam) ? $arrexam['exam_desc'] : ''?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Office</label>
						<div class="col-6">
							<select class="form-control kt-selectpicker" name="seloffice" id="seloffice" <?=$action=='delete'?'disabled':''?>>
								<option value="">-- SELECT OFFICE --</option>
								<?php foreach($arroffice as $office):
										$selected = isset($arrexam) ? $arrexam['office_id'] == $office['office_id'] ? 'selected' : '' : '';?>
									<option value="<?=$office['office_id']?>" <?=$selected?>><?=strtoupper($office['office_code']).' - '.$office['office_desc']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Passing Percentage</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txtpassper" name="txtpassper" value="<?=isset($arrexam) ? $arrexam['passing_percentage'] : ''?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label"></label>
						<div class="col-6">
							<label class="kt-checkbox" style="margin-right: 30px;">
								<input type="checkbox" name="chkfinal" id="chkfinal" <?=isset($arrexam) ? ($arrexam['is_final'] ? 'checked' : '') : ''?>> Final
								<span></span>
							</label>
							<label class="kt-checkbox">
								<input type="checkbox" name="chkpublish" id="chkpublish" <?=isset($arrexam) ? ($arrexam['is_published'] ? 'checked' : '') : 'checked'?>> Publish
								<span></span>
							</label>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-exam">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('exam')?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('select','form_validation'))?>
<script src="<?=base_url('assets/js/custom/exam.js')?>"></script>