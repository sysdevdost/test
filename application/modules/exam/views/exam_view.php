<style type="text/css">
	.nav-link.dropdown-toggle:after, .btn.dropdown-toggle:after {
    	content: '' !important;
	}
	.kt-notification.kt-notification--fit {
	    width: 500px !important;
	}
	i.la.la-ellipsis-h {
	    font-weight: bold !important;
	    color: #2786fb !important;
	    font-size: 20px !important;
	}
	li#dpsub {
		position: relative;
	}
	li#dpsub ul#dpmenu {
		top: 0;
		left: -100%;
		margin-top: -1px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<b>#<?=fix_id($this->uri->segment(3))?>: <?=$exam['exam_title']?></b>
				</h3>
			</div>
			<div class="kt-portlet__head-label">
				<a href="" class="btn btn-brand"><i class="flaticon2-next"></i> Run</a>&nbsp;&nbsp;&nbsp;
				<div class="dropdown">
					<button class="btn btn-brand btn-elevate btn-icon" type="button" data-toggle="dropdown"><i class="flaticon2-gear"></i></button>
				    <ul class="dropdown-menu" id="dpmenu">
				    	<li><a tabindex="-1" href="javascript:;"> Settings </a></li>
				    	<li><a tabindex="-1" href="javascript:;"> Print Preview </a></li>
				    	<li><a tabindex="-1" href="javascript:;"> Delete </a></li>
				    	<li><a tabindex="-1" href="javascript:;"> Candidates </a></li>
				    	<li><a tabindex="-1" href="javascript:;"> Results </a></li>
				    	<li class="dropdown-divider"></li>
				    	<li><a tabindex="-1" href="javascript:;"> Add Category </a></li>
				    	<li class="dropdown-submenu" id="dpsub">
				    		<a class="test" tabindex="-1" href="javascript:;">New Question</a>
				    		<ul class="dropdown-menu" id="dpmenu">
				    			<?php foreach($exam['question_set'] as $question_set): ?>
				    				<li class="dropdown-submenu" id="dpsub">
					    				<a class="test test_categ" href="javascript:;" data-desc="categ" data-id="<?=$question_set['ques_set_id']?>"><?=$question_set['ques_set_name']?></a>
					    				<ul class="dropdown-menu" id="dpmenu">
					    					<?php foreach($quest_types as $type): ?>
						    					<li><a class="test_type" href="javascript:;" data-desc="type" data-id="<?=$type['ques_type_id']?>"><?=$type['ques_type_desc']?></a></li>
						    				<?php endforeach; ?>
					    				</ul>
					    			</li>
				    			<?php endforeach; ?>
				    		</ul>
				    	</li>
				    </ul>
				</div>
				<div hidden>
					<input type="hidden" id="txtcateg"><br>
					<input type="hidden" id="txtquest_type">
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table" id="kt_table-exam">
				<thead>
					<tr>
						<th>#</th>
						<th>Question</th>
						<th>Type</th>
						<th>Answer</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($exam['question_set'] as $question_set): ?>
						<tr class="exam-row">
							<td>
								<a href="javascript:;" class="detail-open" data-question='<?=json_encode($question_set["question_item"])?>'
									data-set_type='<?=$question_set["ques_type_desc"]?>'>
									<i class="fa fa-caret-right"></i></a><?=str_repeat('&nbsp;',5)?>
								Part <?=$no++?>
							</td>
							<td><?=$question_set['ques_set_name']?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>

<?php include('_modal.php'); ?>
<script src="<?=base_url('assets/js/view_exam.js')?>"></script>