<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon-file-2"></i>
				</span>
				<h3 class="kt-portlet__head-title">Exam</h3>
			</div>
			<div class="kt-portlet__head-toolbar" <?=access_level()=='examinee'?'hidden':''?>>
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<a href="<?=base_url('exam/add')?>" class="btn btn-brand btn-elevate btn-sm">
							<i class="la la-plus"></i>
							Add Exam
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="row">
				<?php foreach($arrexams as $exam): ?>
					<div class="col-xl-3">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head kt-portlet__head--noborder">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">
										<?=strtoupper($exam['exam_title'])?></h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<small>
										<a href="<?=$_SESSION['sess_acc_lvl']==3 ? base_url('exam/take_exam/'.$exam['exam_id']) : base_url('exam/view_exam/'.$exam['exam_id'])?>" class="btn btn-success btn-elevate btn-sm" title="Click to view Exam">
											<i class="flaticon-search"></i></a></small>
									<a href="#" class="btn btn-icon" data-toggle="dropdown" <?=access_level()=='examinee'?'hidden':''?>>
										<i class="flaticon-more-1 kt-font-brand"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__item">
												<a href="<?=base_url('examiner/manage/'.$exam['exam_id'])?>" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon-users"></i>
													<span class="kt-nav__link-text">Manage Examiner</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="<?=base_url('examinee/manage/'.$exam['exam_id'])?>" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon-user-add"></i>
													<span class="kt-nav__link-text">Manage Examinee</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="<?=base_url('exam/edit/'.$exam['exam_id'])?>" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon-edit"></i>
													<span class="kt-nav__link-text">Edit Exam</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="<?=base_url('exam/publish/'.$exam['exam_id'].'/'.$exam['is_published'])?>" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon2-check-mark"></i>
													<span class="kt-nav__link-text"><?=$exam['is_published']?'Unpublish':'Publish'?> Exam</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="javascript:;" class="kt-nav__link link-delete" data-id="<?=$exam['exam_id']?>">
													<i class="kt-nav__link-icon flaticon2-trash"></i>
													<span class="kt-nav__link-text">Remove Exam</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body" style="padding-top: 0px;">
								<div class="kt-widget kt-widget--user-profile-2">
									<div class="kt-widget__body">
										<div class="kt-widget__item">
											<div class="kt-widget__contact" <?=access_level()=='examinee'?'hidden':''?>>
												<span class="kt-widget__label">Created By:</span>
												<span class="kt-widget__data"><?=fix_fullname($exam['user_firstname'], $exam['user_lastname'], $exam['user_middlename'], $exam['user_extname'])?></span>
											</div>
											<div class="kt-widget__contact" <?=access_level()=='examinee'?'':'hidden'?>>
												<span class="kt-widget__label">Examiner/s:</span>
												<span class="kt-widget__data"><?=ucwords($exam['office_desc'])?></span>
											</div>
											<div class="kt-widget__contact">
												<span class="kt-widget__label">Office:</span>
												<span class="kt-widget__data"><?=ucwords($exam['office_desc'])?></span>
											</div>
											<div class="kt-widget__contact">
												<span class="kt-widget__label">Status:</span>
												<span class="kt-widget__data"><?=$exam['is_published']?'Published':'Draft'?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="delete-exam" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Delete Exam</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<?=form_open('exam/delete',array('id' => 'frmdelete-exam'))?>
				<div class="modal-body">
					<input type="hidden" id="txtexamid" name="txtexamid">
					Are you sure you want to delete this data?
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-brand">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>
<!--end::Modal-->

<script>
	$(document).ready(function() {
		$('a.link-delete').click(function() {
			$('#txtexamid').val($(this).data('id'));
			$('#delete-exam').modal('show');
		});
	});
</script>
