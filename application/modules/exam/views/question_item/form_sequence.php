<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-file-alt"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> Question Item
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div id="div-item">
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Item 1</label>
							<div class="col-6">
								<textarea class="form-control seq-item" name="txtitem[]"></textarea>
								<button id="btnadd_item">+</button>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">TYPE</label>
						<div class="col-6">
							<select class="form-control kt-selectpicker" name="seloffice" id="seloffice" <?=$action=='delete'?'disabled':''?>>
								<option value="">-- SELECT TYPE --</option>
								<?php foreach($sequence_type as $key=>$type):
										#$selected = isset($arrexam) ? $arrexam['office_id'] == $office['office_id'] ? 'selected' : '' : '';?>
									<option value="<?=$key?>"><?=$type.' - '.$key?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-question-item">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('exam/view_exam/'.$this->uri->segment(4).'/'.$this->uri->segment(5))?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('form_validation'))?>
<script src="<?=base_url('assets/js/custom/question_item-sequence.js')?>"></script>