<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-file-alt"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> Question Item
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Item</label>
						<div class="col-6">
							<textarea class="form-control" id="txtitem" name="txtitem" <?=$action=='delete'?'disabled':''?>><?=isset($question_item) ? $question_item['question_item'] : set_value('txtitem')?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Minimum Answer</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txtans_min" name="txtans_min" value="<?=isset($question_item) ? $question_item['min_answer'] : (count($_POST) > 0 ? set_value('txtans_min') : 1)?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Maximum Answer</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txtans_max" name="txtans_max" value="<?=isset($question_item) ? $question_item['max_answer'] : (count($_POST) > 0 ? set_value('txtans_max') : 1)?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Point/s</label>
						<div class="col-2">
							<input class="form-control" type="number" id="txtans_pts" name="txtans_pts" value="<?=isset($question_item) ? $question_item['answer_points'] : (count($_POST) > 0 ? set_value('txtans_pts') : 1)?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<?php if($question_set['is_subjective']): ?>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Number of items</label>
							<div class="col-2">
								<input class="form-control" type="number" id="txtno_items" name="txtno_items" value="<?=isset($question_item) ? $question_item['no_of_items'] : (count($_POST) > 0 ? set_value('txtno_items') : 1)?>" <?=$action=='delete'?'disabled':''?>>
							</div>
						</div>
						<div class="form-group row">
							<label for="example-text-input" class="col-2 col-form-label">Remarks / Answer</label>
							<div class="col-6">
								<textarea class="form-control" name="txtremarks"></textarea>
							</div>
						</div>
					<?php endif; ?>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label"></label>
						<div class="col-6">
							<label class="kt-checkbox" style="margin-right: 30px;">
								<input type="checkbox" name="chkrandom" id="chkrandom"
										<?=isset($question_item) ? ($question_item['choices_random']==1 ? 'checked' : '') : 'checked'?>> Random
								<span></span>
							</label>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-question-item">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('exam/view_exam/'.$this->uri->segment(4).'/'.$this->uri->segment(5))?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('form_validation'))?>
<script src="<?=base_url('assets/js/custom/question_item.js')?>"></script>