<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="flaticon2-gear"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Test Settings
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin::Form-->
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<pre><?php print_r($arrexam) ?></pre>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">ID No.</label>
						<div class="col-2">
							<input class="form-control" type="text" id="txtexam_id" name="txtexam_id" value="<?=fix_id($_GET['examid'])?>" disabled>
						</div>
					</div>

					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Title</label>
						<div class="col-6">
							<input type="text" class="form-control" name="txtexam_title" id="txtexam_title" value="<?=isset($arrexam) ? $arrexam['exam_title'] : ''?>">
						</div>
					</div>

					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Description</label>
						<div class="col-6">
							<textarea class="form-control" name="txtexam_title" id="txtexam_title"><?=isset($arrexam) ? $arrexam['exam_desc'] : ''?></textarea>
						</div>
					</div>

					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label"><i>COPY TEST FROM</i></label>
						<div class="col-6">
							<select class="form-control kt-select2" name="seloffice" id="seloffice" <?=$action=='delete'?'disabled':''?>>
								<option value="">-- SELECT OFFICE --</option>
								<?php foreach($exam_list as $exam):
										$selected = isset($arrexam) ? $arrexam['exam_id'] == $exam['exam_id'] ? 'selected' : '' : '';?>
									<option value="<?=$exam['exam_id']?>" <?=$selected?>><?=$exam['exam_title']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-exam">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('exam')?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>
