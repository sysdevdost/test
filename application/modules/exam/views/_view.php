<?php $set_tab = $this->uri->segment(4); ?>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet__body">
		<div class="kt-portlet ">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						<?=ucwords($exam['exam_title'])?>
						<small><?=ucfirst($exam['exam_desc'])?></small>		
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							<a href="<?=base_url('exam/question_set/add/'.$this->uri->segment(3))?>" class="btn btn-brand btn-elevate btn-sm">
								<i class="la la-plus"></i>
								Add Question Set
							</a>
						</div>
					</div>
				</div>
				<br>
				<!-- begin question set -->
				<div class="accordion accordion-light  accordion-toggle-arrow" id="acc_set<?=$exam['exam_id']?>">
					<?php
						foreach($exam['question_set'] as $key => $set):
							$card_title = $set_tab != '' ? ($set['ques_set_id'] == $set_tab ? '' : 'collapsed') : ($key == 0 ? '' : 'collapsed');
							$expanded = $set_tab != '' ? ($set['ques_set_id'] == $set_tab ? 'true' : 'false') : ($key == 0 ? 'true' : 'false');
							$show = $set_tab != '' ? ($set['ques_set_id'] == $set_tab ? 'show' : '') : ($key == 0 ? 'show' : '');
						?>
						<div class="card">
							<div class="card-header" id="heading<?=$set['ques_set_id']?>">
								<div class="card-title <?=$card_title?>"
										data-toggle="collapse" 
										data-target="#coll_set<?=$set['ques_set_id']?>" 
										aria-expanded="<?=$expanded?>" 
										aria-controls="coll_set<?=$set['ques_set_id']?>">
									<small><i class="fa fa-circle"></i> </small>&nbsp; <?=ucwords($set['ques_type_desc'])?>
								</div>
							</div>

							<div id="coll_set<?=$set['ques_set_id']?>" 
									class="collapse <?=$show?>" 
									aria-labelledby="heading<?=$set['ques_set_id']?>" 
									data-parent="#acc_set<?=$exam['exam_id']?>">

								<div class="card-body">
									<div class="col-sm-12">
										<a href="<?=base_url('exam/question_item/add/'.$exam['exam_id'].'/'.$set['ques_set_id'])?>"
											class="btn btn-success btn-elevate btn-sm pull-right"
											data-container="body" data-toggle="tooltip" data-placement="top" title="Edit User">
											<i class="la la-plus"></i> Add Question
										</a>
										<div class="kt-section">
											<!-- question set instruction -->
											<div class="kt-section__title" style="font-size: 14px;">
												<?=ucfirst($set['ques_set_instruction'])?>
												<a href="<?=base_url('exam/question_set/edit/'.$exam['exam_id'].'/'.$set['ques_set_id'])?>"
													class="btn btn-brand btn-sm btn-icon btn-circle"><i class="fa fa-pencil-alt"></i></a>&nbsp;
												<a href="javascript:;" class="btn btn-google btn-sm btn-icon btn-circle btndelete-set" data-id="<?=$set['ques_set_id']?>">
													<i class="flaticon-delete"></i></a>
											</div>
											<!-- begin question -->
											<?php $qno = 1; foreach($set['question_item'] as $key=>$ques_item): ?>
												<div class="kt-section__desc">
													<?=$qno++.'. '.ucfirst($ques_item['question_item'])?>
													&nbsp;
													<a href="<?=base_url('exam/question_item/edit/'.$exam['exam_id'].'/'.$set['ques_set_id'].'/'.$ques_item['qitem_id'])?>" style="color:#22b9ff;"><i class="fa fa-pencil-alt"></i></a>
													&nbsp;
													<a href="javascript:;" style="color:red;" class="btndelete-ques" data-id="<?=$ques_item['qitem_id']?>"><i class="fa fa-trash"></i></a>
													<?=$ques_item['remarks_answer']!='' ? '<br><small>'.$ques_item['remarks_answer'].'</small>' : ''?>
												</div>
												<!-- begin choices -->
												<div class="kt-section__content" <?=$set['is_subjective']==1 ? 'hidden': ''?>>
													<?php include('_choices.php'); ?>
												</div>
												<!-- end choices -->
												<?php if($key < count($set['question_item'])-1): ?>
													<div class="kt-separator kt-separator--border-dashed"></div>
												<?php endif; ?>
											<?php endforeach; ?>
											<!-- end question -->

										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<!-- end question set -->

			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="modal-delete-set" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-label">Delete Exam</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<?=form_open('',array('id' => 'frmdelete-set'))?>
				<div class="modal-body">
					<input type="hidden" id="txtsetid" name="txtsetid">
					<input type="hidden" id="txtqitem" name="txtqitem">
					<input type="hidden" id="txtchoiceid" name="txtchoiceid">
					Are you sure you want to delete this data?
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-brand">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>
<!--end::Modal-->

<script>
	$(document).ready(function() {
		$('a.btndelete-set').click(function() {
			var set = $(this).data('id');
			$('#frmdelete-set').attr("action","<?=base_url('exam/question_set/delete/'.$exam['exam_id'])?>");
			$('#txtsetid').val(set);
			$('#modal-label').html('Delete Set');
			$('#modal-delete-set').modal('show');
		});

		$('a.btndelete-ques').click(function() {
			var qitem = $(this).data('id');
			$('#frmdelete-set').attr("action","<?=base_url('exam/question_item/delete/'.$exam['exam_id'])?>");
			$('#txtqitem').val(qitem);
			$('#modal-label').html('Delete Question');
			$('#modal-delete-set').modal('show');
		});

		$('a.btndelete-choice').click(function() {
			var choiceid = $(this).data('id');
			$('#frmdelete-set').attr("action","<?=base_url('exam/choice/delete/'.$exam['exam_id'])?>");
			$('#txtchoiceid').val(choiceid);
			$('#modal-label').html('Delete Choice');
			$('#modal-delete-set').modal('show');
		});
	});
</script>