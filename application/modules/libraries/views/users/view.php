<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-users"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Users
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="dropdown dropdown-inline">
						<a class="btn btn-brand btn-sm" href="<?=base_url('libraries/users/add_user')?>">
							<i class="flaticon2-plus"></i> Add New
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<table class="table table-striped table-bordered" id="table-users" >
				<thead>
					<tr>
						<th style="width: 80px;text-align: center;">No</th>
						<th>Name</th>
						<th>Office</th>
						<th>Username</th>
						<th>Access Level</th>
						<th>Status</th>
						<th width="50px;"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td align="center">1</td>
						<td>Name</td>
						<td>Office</td>
						<td>Username</td>
						<td>Access Level</td>
						<td>Status</td>
						<td align="center" nowrap>
							<a class="btn btn-success btn-sm" href="<?=base_url('libraries/users/edit_user')?>">
								<i class="flaticon2-pen"></i> Edit
							</a>
							<a class="btn btn-danger btn-sm" href="<?=base_url('libraries/users/delete_user')?>">
								<i class="flaticon2-trash"></i> Delete
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php #load_plugin('js',array('datatables'));?>

<script>
    // $(document).ready(function() {
    	// $('#table-users').dataTable();
    	// style="display: none"
        // $('#table-income').dataTable( {
        //     "initComplete": function(settings, json) {
        //         $('.loading-image').hide();
        //         $('#table-income').show();},
        //     "columnDefs": [{ "orderable":false, "targets":'no-sort' }]
        // });
    // });
</script>