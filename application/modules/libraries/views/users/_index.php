<!-- begin:: Content -->
<?=load_plugin('css',array('select'))?>
<style type="text/css">
	.kt-datatable__cell.kt-datatable__toggle-detail {display: none;}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<br>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-users"></i>
				</span>
				<h3 class="kt-portlet__head-title">Users</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<a href="<?=base_url('libraries/user/add')?>" class="btn btn-brand btn-elevate btn-sm">
							<i class="la la-plus"></i>
							New User
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-4 order-1 order-xl-2 kt-align-right">
						<a href="#" class="btn btn-default kt-hidden">
							<i class="la la-cart-plus"></i> New Order
						</a>
						<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
					</div>
				</div>
			</div>
			<!--end: Search Form -->
		</div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table-user" id="html_table">
				<thead>
					<tr>
						<th style="width: 5%">No</th>
						<th>Name</th>
						<th>Access Level</th>
						<th>Office</th>
						<th>Username</th>
						<th>Email</th>
						<th style="width: 50%">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($arrusers as $user): ?>
						<tr>
							<td><?=$no++?></td>
							<td><?=fix_fullname($user['user_firstname'], $user['user_lastname'], $user['user_middlename'])?></td>
							<td><?=access_level($user['access_level_id'])?></td>
							<td><?=$user['office_desc']?></td>
							<td><?=$user['user_name']?></td>
							<td><?=$user['user_email']?></td>
							<td align="center" nowrap>
								<a href="<?=base_url('libraries/user/edit/'.$user['user_id'])?>" class="btn btn-success btn-sm" data-container="body" data-toggle="tooltip" data-placement="top" title="Edit User">
									<i class="la la-pencil"></i>
								</a>
								<a href="<?=base_url('libraries/user/delete/'.$user['user_id'])?>" class="btn btn-google btn-sm" data-container="body" data-toggle="tooltip" data-placement="top" title="Delete User">
									<i class="la la-trash-o"></i>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>
<!-- end:: Content -->
<?=load_plugin('js',array('select'))?>
<script src="<?=base_url('assets/custom/user-data.js')?>"></script>