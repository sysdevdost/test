<?=load_plugin('css',array('select','select2'))?>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-user"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> User
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin::Form-->
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<input type="hidden" id="txtaction" value="<?=$action?>">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-2 col-form-label">User from HRMIS</label>
						<div class="col-6">
							<input type="hidden" id="txtbaseurl" value="<?=base_url()?>">
							<select class="form-control kt-select2" id="selhrmis" name="selhrmis" <?=$action=='delete'?'disabled':''?>>
								<option value="null"> -- NOT APPLICABLE -- </option>
								<?php foreach($arrhrmis_users as $user): ?>
									<option value="<?=$user['empNumber']?>"><?=$user['name']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Firstname</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtfirstname" name="txtfirstname" value="<?=isset($arruser) ? $arruser['user_firstname'] : set_value('txtfirstname')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Middlename</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtmidname" name="txtmidname" value="<?=isset($arruser) ? $arruser['user_middlename'] : set_value('txtmidname')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Lastname</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtlastname" name="txtlastname" value="<?=isset($arruser) ? $arruser['user_lastname'] : set_value('txtlastname')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Extension Name</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtextname" name="txtextname" value="<?=isset($arruser) ? $arruser['user_extname'] : set_value('txtextname')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Office</label>
						<div class="col-6">
							<select class="form-control kt-selectpicker" name="seloffice" id="seloffice" <?=$action=='delete'?'disabled':''?>>
								<option value="">-- SELECT OFFICE --</option>
								<?php foreach($arroffice as $office):
										$selected = isset($arruser) ? $arruser['office_id'] == $office['office_id'] ? 'selected' : '' : '';?>
									<option value="<?=$office['office_id']?>" <?=$selected?>><?=strtoupper($office['office_code']).' - '.$office['office_desc']?></option>
								<?php endforeach; ?>
								<option value="0">Others</option>
							</select>
						</div>
					</div>
					<div class="form-group row add-office">
						<label class="col-2 col-form-label">Office Code</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtoffice_code" name="txtoffice_code" value="<?=isset($arruser) ? '' : set_value('txtoffice_code')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row add-office">
						<label class="col-2 col-form-label">Office Description</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtoffice_desc" name="txtoffice_desc" value="<?=isset($arruser) ? '' : set_value('txtoffice_desc')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Age</label>
						<div class="col-6">
							<input type="hidden" id="yearnow" value="<?=date('Y')?>">
							<input class="form-control" type="number" maxlength="2" id="txtage" name="txtage" value="<?=isset($arruser) ? $arruser['user_age'] : set_value('txtage')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row div-gender">
						<label class="col-2 col-form-label">Gender</label>
						<div class="col-6" style="padding-top: 11px;">
							<label class="label-gender">
								<input type="radio" id="chkgenderf" name="gender" value='f'
									<?=isset($arruser) ? $arruser['user_gender'] == 'f' ? 'checked' : '' : set_value('gender')?>
									<?=$action=='delete'?'disabled':''?>> Female
							</label>
							<label class="label-gender">
								<input type="radio" id="chkgenderm" name="gender" value='m'
									<?=isset($arruser) ? $arruser['user_gender'] == 'm' ? 'checked' : '' : set_value('gender')?>
									<?=$action=='delete'?'disabled':''?>> Male
							</label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Email</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtemail" name="txtemail" value="<?=isset($arruser) ? $arruser['user_email'] : set_value('txtemail')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Mobile Number</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtmobileno" name="txtmobileno" value="<?=isset($arruser) ? $arruser['user_mobile_no'] : set_value('txtmobileno')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Access Level</label>
						<div class="col-6" style="padding-top: 11px;">
							<?php foreach($arraccess_level as $acc): ?>
								<label class="label-acc" style="padding-right: 20px;">
									<input type="radio" name="radacc" value="<?=$acc['access_level_id']?>"
										<?=isset($arruser) ? $arruser['access_level_id'] == $acc['access_level_id'] ? 'checked' : '' : ($acc['access_level_id']==3 ? 'checked' : '')?>
										<?=$action=='delete'?'disabled':''?>> <?=$acc['access_level_desc']?>
								</label>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Username</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtuname" name="txtuname" value="<?=isset($arruser) ? $arruser['user_name'] : set_value('txtuname')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-2 col-form-label">Password</label>
						<div class="col-6">
							<input class="form-control" type="password" id="txtpass" name="txtpass" value="" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-user">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('libraries/user')?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('select','select2','form_validation'))?>
<script src="<?=base_url('assets/js/custom/user.js')?>"></script>