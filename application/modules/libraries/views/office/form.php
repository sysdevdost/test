<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-building"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					<?=ucfirst($action)?> Office
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin::Form-->
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Code</label>
						<div class="col-3">
							<input class="form-control" type="text" id="txtcode" name="txtcode" value="<?=isset($arroffice) ? $arroffice['office_code'] : set_value('txtcode')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Description</label>
						<div class="col-6">
							<input class="form-control" type="text" id="txtdesc" name="txtdesc" value="<?=isset($arroffice) ? $arroffice['office_desc'] : set_value('txtdesc')?>" <?=$action=='delete'?'disabled':''?>>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-<?=$action=='delete'?'google':'brand'?> btn-sm" id="submit-office">
									<i class="flaticon2-<?=$action=='delete'?'trash':'check-mark'?>"></i> <?=$action=='add'?'Submit':$action=='edit'?'Save':'Delete'?></button>
								<a href="<?=base_url('libraries/office')?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->

		</div>
	</div>
</div>

<?=load_plugin('js',array('jquery','form_validation'))?>
<script src="<?=base_url('assets/js/custom/office.js')?>"></script>