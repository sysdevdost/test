<!-- begin:: Content -->
<?=load_plugin('css',array('select'))?>
<style type="text/css">
	.kt-datatable__cell.kt-datatable__toggle-detail {display: none;}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<br>
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand fa fa-building"></i>
				</span>
				<h3 class="kt-portlet__head-title">Office</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<a href="<?=base_url('libraries/office/add')?>" class="btn btn-brand btn-elevate btn-sm">
							<i class="la la-plus"></i>
							New Office
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end: Search Form -->
		</div>
		<div class="kt-portlet__body">
			<!--begin: Datatable -->
			<table class="table-office" id="html_table">
				<thead>
					<tr>
						<th>No</th>
						<th>Code</th>
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($arroffice as $office): ?>
						<tr>
							<td><?=$no++?></td>
							<td><?=$office['office_code']?></td>
							<td><?=$office['office_desc']?></td>
							<td align="center" nowrap>
								<a href="<?=base_url('libraries/office/edit/'.$office['office_id'])?>" class="btn btn-success btn-sm">
									<i class="la la-plus"></i>
									Edit
								</a>
								<a href="<?=base_url('libraries/office/delete/'.$office['office_id'])?>" class="btn btn-google btn-sm">
									<i class="la la-trash-o"></i>
									Delete
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>
<!-- end:: Content -->
<?=load_plugin('js',array('select'))?>
<script src="<?=base_url('assets/custom/office-data.js')?>"></script>