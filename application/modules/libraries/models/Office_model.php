<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Office_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($officeid='')
	{
		if($officeid!=''):
			$res = $this->db->get_where('tbloffice',array('office_id' => $officeid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->get('tbloffice')->result_array();
		endif;
	}

	public function getdataby_code($code)
	{
		$this->db->where('LOWER(office_code)',$code);
		$res = $this->db->get('tbloffice')->result_array();
		return count($res) > 0 ? $res[0] : array();
	}

	public function addoffice($arrdata)
	{
		$this->db->insert('tbloffice',$arrdata);
		return $this->db->insert_id();
	}

	public function editoffice($arrdata,$officeid)
	{
		$this->db->where('office_id',$officeid);
		$this->db->update('tbloffice',$arrdata);
		return $this->db->affected_rows();
	}

	public function deleteoffice($officeid)
	{
		$this->db->where('office_id',$officeid);
		$this->db->delete('tbloffice'); 	
		return $this->db->affected_rows();
	}

	public function check_exist($code)
	{
		$res = $this->db->get_where('tbloffice', array('office_code' => $code))->result_array();

		return count($res) > 0 ? 1 : 0;
	}

}