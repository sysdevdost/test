<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($userid='')
	{
		if($userid!=''):
			$res = $this->db->join('tbluserprofile','tbluserprofile.user_id = tblusers.user_id')
						 ->join('tbloffice','tbloffice.office_id = tblusers.office_id')
						 ->get_where('tblusers',array('tblusers.user_id' => $userid))->result_array();

			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->join('tbluserprofile','tbluserprofile.user_id = tblusers.user_id')
						 ->join('tbloffice','tbloffice.office_id = tblusers.office_id')
						 ->get_where('tblusers',array('tblusers.is_delete' => 0))->result_array();
		endif;
	}

	public function authenticate($username,$pass)
	{
		$user = $this->db->join('tbluserprofile','tbluserprofile.user_id = tblusers.user_id')
						 ->get_where('tblusers',array('tblusers.user_name' => $username))->result_array();	

		if(count($user) > 0):
			if(password_verify($pass,$user[0]['user_password'])):
				return $user[0];
			endif;
		endif;

		return array();
	}

	function set_session($arrdata)
	{
		$sessdata = array(
			 'sess_userid'		=> $arrdata['user_id'],
			 'sess_hrmis_empno' => $arrdata['hrmis_empnumber'],
			 'sess_acc_lvl'  	=> $arrdata['access_level_id'],
			 'sess_office'		=> $arrdata['office_id'],
			 'sess_uname'	  	=> $arrdata['user_name'],
			 'sess_name'  		=> fix_fullname($arrdata['user_firstname'], $arrdata['user_lastname'], $arrdata['user_middlename'], $arrdata['user_extname']),
			 'sess_email'		=> $arrdata['user_email']);

		$this->session->set_userdata($sessdata);
	}

	public function adduser($arrdata)
	{
		$this->db->insert('tblusers', $arrdata);
		return $this->db->insert_id();
	}

	public function edituser($arrdata,$userid)
	{
		$this->db->where('user_id',$userid);
		$this->db->update('tblusers',$arrdata);
		return $this->db->affected_rows();
	}

	public function addprofile($arrdata)
	{
		$this->db->insert('tbluserprofile',$arrdata);
		return $this->db->insert_id();
	}

	public function editprofile($arrdata,$userid)
	{
		$this->db->where('user_id',$userid);
		$this->db->update('tbluserprofile',$arrdata);
		return $this->db->affected_rows();
	}

	public function check_username_exists($username)
	{
		return $this->db->get_where('tblusers',array('user_name' => $username))->result_array();
	}

	function gethrmis_users()
	{
		// $this->load->library('nusoap_lib');
		// $namespace = 'http://hrmis.dost.gov.ph/xml/employee.soap.php';
		// $this->client = new nusoap_client($namespace);
		// $this->userLists = $this->client->call('getEmployee',array('fingerprint'=>'!+D$0@9'));
		// return json_decode($this->userLists,true);

		$arrusers = file_get_contents('http://192.168.33.10/playground/hrmis.php');
		return json_decode($arrusers,true);
		
	}

	function getaccess_level()
	{
		return $this->db->get('tblaccesslevel')->result_array();
	}

}