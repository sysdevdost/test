<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('libraries/User_model','libraries/Office_model'));
    }

	public function index()
	{
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['arrusers'] = $this->User_model->getdata();
		$this->template->load('template/template-main', 'libraries/users/_index', $this->arrdata);
	}

	public function add()
	{
		$arrpost = $this->input->post();
		
		if(!empty($arrpost)):
			# add office
			if($arrpost['seloffice'] == '0'):
				$arrdata_office = array(
									'office_code'  => $arrpost['txtoffice_code'],
									'office_desc'  => $arrpost['txtoffice_desc'],
									'created_by'   => $_SESSION['sess_userid'],
									'created_date' => date('Y-m-d H:i:s'));
				$office_id = $this->Office_model->addoffice($arrdata_office);
			else:
				$office_id = $arrpost['seloffice'];
			endif;

			$arrdata_user = array(
								'user_name'		  => $arrpost['txtuname'],
								'user_password'	  => password_hash($arrpost['txtpass'],PASSWORD_BCRYPT),
								'hrmis_empnumber' => '',
								'access_level_id' => 1,
								'office_id' 	  => $office_id,
								'created_by' 	  => $_SESSION['sess_userid'],
								'created_date' 	  => date('Y-m-d H:i:s'));
			$user_id = $this->User_model->adduser($arrdata_user);

			# add user profile
			$arrdata_profile = array(
									'user_id' 		 => $user_id,
									'user_firstname' => $arrpost['txtfirstname'],
									'user_middlename'=> $arrpost['txtmidname'],
									'user_lastname'	 => $arrpost['txtlastname'],
									'user_extname'	 => $arrpost['txtextname'],
									'user_age'		 => $arrpost['txtage'],
									'user_gender'	 => $arrpost['gender'],
									'user_email'	 => $arrpost['txtemail'],
									'user_mobile_no' => $arrpost['txtmobileno']);
			$profileid = $this->User_model->addprofile($arrdata_profile);
			
			$this->session->set_flashdata('strSuccessMsg','User successfully added.');
			redirect('libraries/user');
		endif;
		$this->arrdata['action'] = 'add';
		$this->arrdata['arrhrmis_users'] = $this->User_model->gethrmis_users();
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['arraccess_level'] = $this->User_model->getaccess_level();
		$this->template->load('template/template-main', 'libraries/users/form', $this->arrdata);
	}

	public function edit()
	{
		$userid = $this->uri->segment(4);
		$arrpost = $this->input->post();

		if(!empty($arrpost)):
			# add office
			if($arrpost['seloffice'] == 'others'):
				$arrdata_office = array(
									'office_code'  => $arrpost['txtoffice_code'],
									'office_desc'  => $arrpost['txtoffice_desc'],
									'created_by'   => $_SESSION['sess_userid'],
									'created_date' => date('Y-m-d H:i:s'));
				$office_id = $this->Office_model->addoffice($arrdata_office);
			else:
				$office_id = $arrpost['seloffice'];
			endif;

			$arrdata_user = array(
								'access_level_id' => $arrpost['radacc'],
								'office_id' 	  => $office_id,
								'created_by' 	  => $_SESSION['sess_userid'],
								'created_date' 	  => date('Y-m-d H:i:s'));
			if($arrpost['txtpass']!=''):
				$arrdata_user['user_password'] = password_hash($arrpost['txtpass'],PASSWORD_BCRYPT);
			endif;
			$user_id = $this->User_model->edituser($arrdata_user,$userid);

			# add user profile
			$arrdata_profile = array(
									'user_firstname' => $arrpost['txtfirstname'],
									'user_middlename'=> $arrpost['txtmidname'],
									'user_lastname'	 => $arrpost['txtlastname'],
									'user_extname'	 => $arrpost['txtextname'],
									'user_age'		 => $arrpost['txtage'],
									'user_gender'	 => $arrpost['gender'],
									'user_email'	 => $arrpost['txtemail'],
									'user_mobile_no' => $arrpost['txtmobileno']);
			$profileid = $this->User_model->editprofile($arrdata_profile,$userid);
			
			$this->session->set_flashdata('strSuccessMsg','User successfully updated.');
			redirect('libraries/user');
		endif;
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['arruser'] = $this->User_model->getdata($userid);
		$this->arrdata['arraccess_level'] = $this->User_model->getaccess_level();
		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'libraries/users/form', $this->arrdata);
	}

	public function delete()
	{
		$userid = $this->uri->segment(4);
		$arrpost = $this->input->post();

		if(!empty($arrpost)):
			$this->User_model->edituser(array('is_delete' => 1),$userid);

			$this->session->set_flashdata('strSuccessMsg','User successfully deleted.');
			redirect('libraries/user');
		endif;
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->arrdata['arruser'] = $this->User_model->getdata($userid);
		$this->arrdata['action'] = 'delete';
		$this->template->load('template/template-main', 'libraries/users/form', $this->arrdata);
	}
	
	public function register()
	{
		$arrpost = $this->input->post();
		$office_id = 0;
		if(!empty($arrpost)):
			# check username if exists
			if(count($this->User_model->check_username_exists($arrpost['username'])) > 0):
				$this->session->set_flashdata('strErrorMsg','Username already registered.');
				$this->arrdata['for_registration'] = 1;
				$this->arrdata['arrhrmis_users'] = $this->User_model->gethrmis_users();
				$this->arrdata['arroffice'] = $this->Office_model->getdata();
				$this->arrdata['arraccess_level'] = $this->User_model->getaccess_level();
				$this->template->load('template/template-login', 'login/login_view', $this->arrData);
			else:
				# add office
				if($arrpost['seloffice'] == 'others'):
					$arrdata_office = array(
										'office_code'  => $arrpost['txtoffice_code'],
										'office_desc'  => $arrpost['txtoffice_desc'],
										'created_date' => date('Y-m-d H:i:s'));
					$office_id = $this->Office_model->addoffice($arrdata_office);
				else:
					$office_id = $arrpost['seloffice'];
				endif;

				$arrdata_user = array(
									'user_name'		  => $arrpost['username'],
									'user_password'	  => password_hash($arrpost['password'],PASSWORD_BCRYPT),
									'hrmis_empnumber' => '',
									'access_level_id' => 1,
									'office_id' 	  => $office_id,
									'created_date' 	  => date('Y-m-d H:i:s'));
				$user_id = $this->User_model->adduser($arrdata_user);

				# update office after the user created and if office is others
				if($arrpost['seloffice'] == 'others'):
					$this->Office_model->editoffice(array( 'created_by' => $user_id),$office_id);
				endif;

				# update created by of user
				$this->User_model->edituser(array( 'created_by' => $user_id),$user_id);

				# add user profile
				$arrdata_profile = array(
										'user_id' 		 => $user_id,
										'user_firstname' => $arrpost['fname'],
										'user_middlename'=> $arrpost['mname'],
										'user_lastname'	 => $arrpost['lname'],
										'user_extname'	 => $arrpost['extname'],
										'user_age'		 => $arrpost['age'],
										'user_gender'	 => $arrpost['gender'],
										'user_email'	 => $arrpost['email'],
										'user_mobile_no' => $arrpost['mobileno']);
				$profileid = $this->User_model->addprofile($arrdata_profile);

				$this->session->set_flashdata('strSuccessMsg','You are successfully registered.');
				redirect('login');
			endif;
		endif;

		
	}

	public function api_user()
	{
		$userLists = '';

		$empid = isset($_GET['empid']) ? $_GET['empid'] : '';

		$this->load->library('nusoap_lib');
		$namespace = $_ENV['HRMIS_LINK'].'/api/userxml.php?wsdl';
		$this->client = new nusoap_client($namespace);
		$this->client->setCredentials("hrmisv10", "!7D$0@9");
		$userLists = $this->client->call('getData',array('empid'=>$empid));

		echo $userLists;
	}

	public function hrmis_login()
	{
		$userLists = '';

		$uname = isset($_GET['uname']) ? $_GET['uname'] : '';
		$pass = isset($_GET['pass']) ? $_GET['pass'] : '';

		$this->load->library('nusoap_lib');
		$namespace = $_ENV['HRMIS_LINK'].'/api/hrmislogin.php?wsdl';
		$this->client = new nusoap_client($namespace);
		$this->client->setCredentials("hrmisv10", "!7D$0@9");
		$userLists = $this->client->call('getData',array('uname'=>$uname,'pass'=>$pass));

		echo $userLists;
	}
	

}
