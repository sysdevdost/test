<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('libraries/Office_model'));
    }

	public function index()
	{
		$this->arrdata['arroffice'] = $this->Office_model->getdata();
		$this->template->load('template/template-main', 'libraries/office/_index', $this->arrdata);
	}

	public function add()
	{
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$arrdata_office = array(
								'office_code'	=> $arrpost['txtcode'],
								'office_desc'	=> $arrpost['txtdesc'],
								'created_by'	=> $_SESSION['sess_userid'],
								'created_date'	=> date('Y-m-d H:i:s')
								);
			# check if office exist
			if($this->Office_model->check_exist($arrpost['txtcode'])):
				$this->session->set_flashdata('strErrorMsg','Office already exists.');
			else:
				$this->Office_model->addoffice($arrdata_office);
				$this->session->set_flashdata('strSuccessMsg','Office added successfully.');
				redirect('libraries/office');
			endif;
		endif;
		$this->arrdata['action'] = 'add';
		$this->template->load('template/template-main', 'libraries/office/form', $this->arrdata);
	}

	public function edit()
	{
		$arrpost = $this->input->post();
		$officeid = $this->uri->segment(4);
		if(!empty($arrpost)):
			$arrdata_office = array(
								'office_code'	=> $arrpost['txtcode'],
								'office_desc'	=> $arrpost['txtdesc'],
								'last_updated_by'  => $_SESSION['sess_userid'],
								'last_updated_date'=> date('Y-m-d H:i:s')
								);

			$this->Office_model->editoffice($arrdata_office,$officeid);
			$this->session->set_flashdata('strSuccessMsg','Office updated successfully.');
			redirect('libraries/office');
		endif;
		$this->arrdata['arroffice'] = $this->Office_model->getdata($officeid);
		$this->arrdata['action'] = 'edit';
		$this->template->load('template/template-main', 'libraries/office/form', $this->arrdata);
	}

	public function delete()
	{
		$arrpost = $this->input->post();
		$officeid = $this->uri->segment(4);
		if(!empty($arrpost)):
			$this->Office_model->deleteoffice($officeid);
			$this->session->set_flashdata('strSuccessMsg','Office deleted successfully.');
			redirect('libraries/office');
		endif;
		$this->arrdata['arroffice'] = $this->Office_model->getdata($officeid);
		$this->arrdata['action'] = 'delete';
		$this->template->load('template/template-main', 'libraries/office/form', $this->arrdata);
	}

	public function json_office()
	{
		$code = isset($_GET['code']) ? $_GET['code'] : '';
		if($code!=''):
			$data = $this->Office_model->getdataby_code($code);
			echo json_encode($data);
		endif;
	}
	
	

}
