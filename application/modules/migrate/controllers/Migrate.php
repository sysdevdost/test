<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends MY_Controller {

	var $arrData;

	function __construct() {
        parent::__construct();
    }

	public function index()
	{
		$this->load->library('migration');

		if (! $this->migration->current())
		{
			 echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully!';
        }   
	
	}
	
	

}
