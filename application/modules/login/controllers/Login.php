<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	var $arrData;

	function __construct() {
		$this->load->model(array('libraries/Office_model','libraries/User_model'));
        parent::__construct();
    }

	public function index()
	{
		$this->arrData['arroffice'] = $this->Office_model->getdata();
		$this->template->load('template/template-login', 'login/login_view', $this->arrData);
	}

	public function authenticate()
	{
		$arrpost = $this->input->post();
		if(!empty($arrpost)):
			$valid_user = $this->User_model->authenticate($arrpost['uname'],$arrpost['password']);

			if(count($valid_user) > 0):
				$this->User_model->set_session($valid_user);
				redirect('dashboard');
			else:
				$this->session->set_flashdata('strErrorMsg','Invalid username / password.');
				redirect('login');
			endif;
		endif;
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}	
	

}
