<?=load_plugin('css',array('select'))?>
<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
	<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?=base_url('assets/media//bg/bg-3.jpg')?>);">
			<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
				<div class="kt-login__container">
					<div class="kt-login__logo">
						<a href="#">
							<img src="<?=base_url('assets/media/logos/logo.png')?>" width="50px">
						</a>
					</div>
					<div class="kt-login__signin">
						<div class="kt-login__head">
							<h3 class="kt-login__title">Sign In</h3>
						</div>
						<input type="hidden" id="txtpost" value="<?=count($_POST) > 0 ? 1 : 0?>">
						<?=form_open('login/authenticate', array('method' => 'post', 'id' => 'frmlogin', 'class' => 'kt-form'))?>
							<div class="form-group validated input-group">
								<input class="form-control" type="text"
									id="txtuname" placeholder="Username" name="uname" autocomplete="off">
							</div>
							<div class="form-group validated input-group">
								<input class="form-control" type="password" 
									id="txtpass" placeholder="Password" name="password" autocomplete="off">								
							</div>
							<div class="row kt-login__extra">
								<div class="col kt-align-right">
									<a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
								</div>
							</div>
							<div class="kt-login__actions">
								<button type="submit" class="btn btn-xs btn-brand btn-elevate" id="btnsign_in">Sign In</button>
							</div>
						<?=form_close()?>
					</div>
					<div class="kt-login__signup">
						<div class="kt-login__head">
							<h3 class="kt-login__title">Sign Up</h3>
							<div class="kt-login__desc">Enter your details to create your account:</div>
						</div>
						<?=form_open('libraries/user/register', array('method' => 'post', 'id' => 'frmregister', 'class' => 'kt-form'))?>
							<div class="input-group">
								<a class="btn btn-xs btn-brand" href="javascript:;" data-toggle="modal" data-target="#modal-login-hrmis"> Login with <u>HRMIS Account</u> </a>
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Firstname" name="fname" id="txtfname" value="<?=count($_POST) > 0 ? $_POST['fname'] : ''?>">
							</div>
							<div class="form-group validated input-group">
								<input class="form-control" type="text" placeholder="Middlename" name="mname" id="txtmname" value="<?=count($_POST) > 0 ? $_POST['mname'] : ''?>">
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Lastname" name="lname" id="txtlname" value="<?=count($_POST) > 0 ? $_POST['lname'] : ''?>">
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Extension Name" name="extname" id="txtextname" value="<?=count($_POST) > 0 ? $_POST['extname'] : ''?>">
							</div>
							<div class="input-group">
								<select class="form-control kt-selectpicker" name="seloffice" id="seloffice">
									<option value="">-- SELECT OFFICE --</option>
									<?php foreach($arroffice as $office): ?>
										<option value="<?=$office['office_id']?>"><?=strtoupper($office['office_code']).' - '.$office['office_desc']?></option>
									<?php endforeach; ?>
									<option value="others">Others</option>
								</select>
							</div>
							<div class="input-group divoffice">
								<input class="form-control" type="text" placeholder="Office Code" name="txtoffice_code"
									id="txtoffice_code" autocomplete="off" value="<?=count($_POST) > 0 ? $_POST['txtoffice_code'] : ''?>">
							</div>
							<div class="input-group divoffice">
								<input class="form-control" type="text" placeholder="Office Description" name="txtoffice_desc"
									id="txtoffice_desc" autocomplete="off" value="<?=count($_POST) > 0 ? $_POST['txtoffice_desc'] : ''?>">
							</div>
							<div class="input-group">
								<input type="hidden" id="yearnow" value="<?=date('Y')?>">
								<input class="form-control" type="text" placeholder="Age" name="age" id="txtage" value="<?=count($_POST) > 0 ? $_POST['age'] : ''?>">
							</div>
							<div class="input-group div-gender">
								<label class="label-gender">Gender</label>
								<label class="label-gender">
									<input type="radio" id="chkgenderf" name="gender" value='f' <?=count($_POST) > 0 ? ($_POST['gender'] == 'f' ? 'checked' : '') : ''?>> Female</label>
								<label class="label-gender">
									<input type="radio" id="chkgenderm" name="gender" value='m' <?=count($_POST) > 0 ? ($_POST['gender'] == 'm' ? 'checked' : '') : ''?>> Male</label>
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Email" name="email" id="txtemail" autocomplete="off" value="<?=count($_POST) > 0 ? $_POST['email'] : ''?>">
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Mobile Number" name="mobileno" id="txtmobileno" value="<?=count($_POST) > 0 ? $_POST['mobileno'] : ''?>">
							</div>
							<div class="input-group">
								<input class="form-control" type="text" placeholder="Username" name="username" id="txtusername" value="<?=count($_POST) > 0 ? $_POST['username'] : ''?>">
							</div>
							<div class="input-group">
								<input class="form-control" type="password" placeholder="Password" name="password" id="txtpassword">
							</div>
							<div class="input-group">
								<input class="form-control" type="password" placeholder="Confirm Password" name="rpassword" id="txtrpassword">
							</div>
							<div class="kt-login__actions">
								<button type="submit" id="btnregister" class="btn btn-xs btn-brand btn-elevatey">Sign Up</button>&nbsp;&nbsp;
								<a href="<?=base_url('login')?>" id="kt_login_signup_cancel" class="btn btn-xs btn-light btn-elevate">Cancel</a>
							</div>
						<?=form_close()?>
					</div>
					<div class="kt-login__forgot">
						<div class="kt-login__head">
							<h3 class="kt-login__title">Forgotten Password ?</h3>
							<div class="kt-login__desc">Enter your email to reset your password:</div>
						</div>
						<?=form_open('login/forget_password', array('method' => 'post', 'id' => 'frmforget_password', 'class' => 'kt-form'))?>
							<div class="form-group validated input-group">
								<input class="form-control" type="text" placeholder="Email" name="email" id="txtforget_email" autocomplete="off">
							</div>
							<div class="kt-login__actions">
								<button type="submit" id="btnforget_password" class="btn btn-brand btn-elevate">Request</button>&nbsp;&nbsp;
								<a href="<?=base_url('login')?>" id="kt_login_forgot_cancel" class="btn btn-light btn-elevate">Cancel</a>
							</div>
						<?=form_close()?>
					</div>
					<div class="kt-login__account">
						<span class="kt-login__account-msg">
							Don't have an account yet ?
						</span>
						&nbsp;&nbsp;
						<a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:: Page -->

<!--begin:: Login Hrmis Account Modal-->
<div class="modal modal-stick-to-bottom fade" id="modal-login-hrmis" role="dialog" data-backdrop="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">HRMIS Account</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<?=form_open('login/hrmis', array('method' => 'post', 'id' => 'frmlogin_hrmis', 'class' => 'kt-form'))?>
				<div class="modal-body">
					<input type="hidden" id="txtbaseurl" value="<?=base_url()?>">
					<div class="form-group validated input-group">
						<input class="form-control" type="text" 
							id="txthrmis-uname" placeholder="Username" name="hrmis_uname" autocomplete="off">
					</div>
					<div class="form-group validated input-group">
						<input class="form-control" type="password" 
							id="txthrmis-pass" placeholder="Password" name="hrmis_password" autocomplete="off">								
					</div>
					<div class="form-group" style="margin-left: 10px;">
						<span class="kt-font-danger" id="spn_invalid_user"></span>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnhrmis_login" class="btn btn-brand">Sign In</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>

<!--end:: Login Hrmis Account Modal-->

<?=load_plugin('js',array('form_validation_login'))?>