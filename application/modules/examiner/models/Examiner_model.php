<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examiner_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getdata($examid,$examinerid='')
	{
		if($examinerid!=''):
			$res = $this->db->join('tbluserprofile','tbluserprofile.user_id = tblexaminer.user_id')
							->get_where('tblexaminer',array('exam_id' => $examid, 'exam_id' => $examinerid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->join('tbluserprofile','tbluserprofile.user_id = tblexaminer.user_id')
							->get_where('tblexaminer',array('exam_id' => $examid, 'is_delete' => 0))->result_array();
		endif;
	}

	public function add($arrdata)
	{
		$this->db->insert('tblexaminer',$arrdata);
		return $this->db->insert_id();
	}

	public function save($arrdata,$examinerid)
	{
		$this->db->where('examiner_id',$examinerid);
		$this->db->update('tblexaminer',$arrdata);
		return $this->db->affected_rows();
	}

	public function check_exist($examinerid,$examid)
	{
		$res = $this->db->get_where('tblexaminer', array('user_id' => $examinerid, 'exam_id' => $examid))->result_array();
		return count($res) > 0 ? $res[0] : array();
	}

}