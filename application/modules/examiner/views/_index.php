<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon-users"></i>
				</span>
				<h3 class="kt-portlet__head-title">Examiner</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<a href="<?=base_url('examiner/add')?>" class="btn btn-brand btn-elevate btn-sm">
							<i class="la la-plus"></i>
							Add Examiner
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="row">
				<?php foreach($arrexaminers as $examiner): ?>
					<div class="col-xl-3">
						<div class="kt-portlet kt-portlet--height-fluid">
							<div class="kt-portlet__head kt-portlet__head--noborder">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title"><?=fix_fullname($examiner['user_firstname'], $examiner['user_lastname'], $examiner['user_middlename'], $examiner['user_extname'])?></h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<a href="#" class="btn btn-icon" data-toggle="dropdown">
										<i class="flaticon-more-1 kt-font-brand"></i>
									</a>
									<div class="dropdown-menu dropdown-menu-right">
										<ul class="kt-nav">
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon2-line-chart"></i>
													<span class="kt-nav__link-text">Reports</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon2-file"></i>
													<span class="kt-nav__link-text">Exam List</span>
												</a>
											</li>
											<li class="kt-nav__item">
												<a href="#" class="kt-nav__link">
													<i class="kt-nav__link-icon flaticon2-trash"></i>
													<span class="kt-nav__link-text">Remove Examiner</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body" style="padding-top: 0px;">
								<div class="kt-widget kt-widget--user-profile-2">
									<div class="kt-widget__body">
										<div class="kt-widget__item">
											<div class="kt-widget__contact">
												<span class="kt-widget__label">Exam Title:</span>
												<span class="kt-widget__data"></span>
											</div>
											<div class="kt-widget__contact">
												<span class="kt-widget__label">Office:</span>
												<span class="kt-widget__data"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>