<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examiner extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Examiner_model','exam/Exam_model','libraries/User_model','libraries/Office_model'));
    }
	
	public function add_exam_examiner()
	{
		$examid = $this->uri->segment(3);
		$arrpost = $this->input->post();

		if(!empty($arrpost)):
			$arrexaminer = array(
							'user_id'	   => $arrpost['selexaminee'],
							'exam_id'	   => $examid,
							'created_by'   => $_SESSION['sess_userid'],
							'created_date' => date('Y-m-d H:i:s')
						);
			
			# check if examiner in exam is already exists
			$examiner_exists = $this->Examiner_model->check_exist($arrpost['selexaminee'],$examid);
			if(count($examiner_exists) > 0):
				if($examiner_exists['is_delete']):
					$this->Examiner_model->save(array('is_delete' => 0),$examiner_exists['examiner_id']);
					$this->session->set_flashdata('strSuccessMsg','Examiner successfully added.');
					redirect('examiner/manage/'.$examid);
				else:
					$this->session->set_flashdata('strErrorMsg','Examiner in exam already exists.');
				endif;
			else:
				$this->Examiner_model->add($arrexaminer);
				$this->session->set_flashdata('strSuccessMsg','Examiner successfully added.');
				redirect('examiner/manage/'.$examid);
			endif;
		endif;

		$this->arrdata['arrexam'] = $this->Exam_model->getdata($examid);
		$this->arrdata['arrusers'] = $this->User_model->getdata();
		$this->arrdata['arroffice'] = $this->Office_model->getdata();

		$this->template->load('template/template-main', 'examiner/form', $this->arrdata);
	}

	# Manage Exam - Examinee
	public function manage()
	{
		$examid = $this->uri->segment(3);

		$this->arrdata['arrexam'] = $this->Exam_model->getdata($examid);
		$this->arrdata['arrexaminer'] = $this->Examiner_model->getdata($examid);
		$this->template->load('template/template-main', 'examiner/_view', $this->arrdata);
	}

	public function remove_examiner()
	{
		$examinerid = $_POST['txtexaminer'];
		$examid = $this->uri->segment(3);
		$this->Examiner_model->save(array('is_delete' => 1),$examinerid);
		$this->session->set_flashdata('strSuccessMsg','Examiner successfully remove.');
		redirect('examiner/manage/'.$examid);
	}


}
