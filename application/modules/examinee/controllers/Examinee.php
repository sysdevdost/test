<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examinee extends MY_Controller {

	var $arrdata;

	function __construct() {
        parent::__construct();
        $this->load->model(array('Examinee_model','libraries/User_model','exam/Exam_model','libraries/Office_model'));
    }

	public function exam()
	{
		$this->arrdata['arrexams'] = $this->Exam_model->getdata();
		$this->template->load('template/template-main', 'exam/_index', $this->arrdata);
	}

	public function add_exam_examinee()
	{
		$examid = $this->uri->segment(3);
		$arrpost = $this->input->post();

		if(!empty($arrpost)):
			$arrexaminee = array(
							'user_id'	   => $arrpost['selexaminee'],
							'exam_id'	   => $examid,
							'created_by'   => $_SESSION['sess_userid'],
							'created_date' => date('Y-m-d H:i:s')
						);
			
			# check if examinee in exam is already exists
			$examinee_exists = $this->Examinee_model->check_exist($arrpost['selexaminee'],$examid);
			if(count($examinee_exists) > 0):
				if($examinee_exists['is_delete']):
					$this->Examinee_model->save(array('is_delete' => 0),$examinee_exists['examinee_id']);
					$this->session->set_flashdata('strSuccessMsg','Examinee successfully added.');
					redirect('examinee/manage/'.$examid);
				else:
					$this->session->set_flashdata('strErrorMsg','Examinee in exam already exists.');
				endif;
			else:
				$this->Examinee_model->add($arrexaminee);
				$this->session->set_flashdata('strSuccessMsg','Examinee successfully added.');
				redirect('examinee/manage/'.$examid);
			endif;
		endif;

		$this->arrdata['arrexam'] = $this->Exam_model->getdata($examid);
		$this->arrdata['arrusers'] = $this->User_model->getdata();
		$this->arrdata['arroffice'] = $this->Office_model->getdata();

		$this->template->load('template/template-main', 'examinee/form', $this->arrdata);
	}

	# Manage Exam - Examinee
	public function manage()
	{
		$examid = $this->uri->segment(3);

		$this->arrdata['arrexam'] = $this->Exam_model->getdata($examid);
		$this->arrdata['arrexaminees'] = $this->Examinee_model->getdata($examid);
		$this->template->load('template/template-main', 'examinee/_view', $this->arrdata);
	}

	public function remove_examinee()
	{
		$examineeid = $_POST['txtexaminee'];
		$examid = $this->uri->segment(3);
		$this->Examinee_model->save(array('is_delete' => 1),$examineeid);
		$this->session->set_flashdata('strSuccessMsg','Examinee successfully remove.');
		redirect('examinee/manage/'.$examid);
	}
	

}
