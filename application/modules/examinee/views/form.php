<?=load_plugin('css',array('select','select2'))?>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand la la-user-plus"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Add Examinee
				</h3>
			</div>
		</div>

		<div class="kt-portlet__body">
			<!-- title -->
			<h4 class="kt-font-brand"><?=$arrexam['exam_title']?></h4>

			<div class="kt-section__content kt-section__content--solid">
				<!-- office name -->
				<h6><?=ucwords($arrexam['office_desc'])?></h6>
			</div>
			<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"
							style="margin-bottom: 0 !important;margin-top: 10px !important;"></div>

			<!--begin::Form-->
			<?=form_open('',array('class' => 'kt-form kt-form--label-right'))?>
				<input type="hidden" name="token_hidden">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<label class="col-2 col-form-label">Office</label>
						<div class="col-6">
							<select class="form-control kt-selectpicker" name="seloffice" id="seloffice">
								<?php foreach($arroffice as $office):
										$selected = isset($_POST) ? (set_value('seloffice') == $office['office_id'] ? 'selected' : '') : ($arrexam['office_id'] == $office['office_id'] ? 'selected' : '');?>
									<option value="<?=$office['office_id']?>" <?=$selected?>><?=strtoupper($office['office_code']).' - '.$office['office_desc']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="form-group row" style="margin-bottom: 0px;">
						<label class="col-2 col-form-label">Examinee</label>
						<div id="div-select2" style="width: 49% !important;margin-left: 10px;">
							<select class="form-control kt-select2" id="selexaminee" name="selexaminee" style="width: 99.9%;">
								<option value="NULL"> -- SELECT EXAMINEE -- </option>
								<?php foreach($arrusers as $user):
										$selected = set_value('seloffice') == $user['user_id'] ? 'selected' : '';?>
										<option value="<?=$user['user_id']?>" <?=$selected?>>
											<?=fix_fullname($user['user_firstname'], $user['user_lastname'], $user['user_middlename'], $user['user_extname'])?>
										</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col-2 col-form-label">&nbsp;</label>
						<div id="error-examinee">&nbsp;&nbsp;<small> Examinee must not be empty.</small></div>
					</div>
				</div>

				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-2"></div>
							<div class="col-10">
								<button type="submit" class="btn btn-brand btn-sm" id="submit-user">
									<i class="flaticon2-check-mark"></i> Submit</button>
								<a href="<?=base_url('examinee/manage/'.$arrexam['exam_id'])?>" class="btn btn-secondary btn-sm"><i class="fa fa-ban"></i> Cancel</a>
							</div>
						</div>
					</div>
				</div>
			<?=form_close()?>
			<!--end::Form-->
		</div>
	</div>
</div>

<?=load_plugin('js',array('select2','select','form_validation'))?>
<script src="<?=base_url('assets/js/custom/add_examinee.js')?>"></script>