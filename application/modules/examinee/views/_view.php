<?=load_plugin('css',array('select'))?>
<style type="text/css">
	.kt-datatable__cell.kt-datatable__toggle-detail {display: none;}
</style>
<br>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon-file-1"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Manage Examinees
				</h3>
			</div>
		</div>
		<div class="kt-portlet__body">
			<!-- title -->
			<h4 class="kt-font-brand"><?=$arrexam['exam_title']?>
				<a href="<?=base_url('exam/view_exam/'.$arrexam['exam_id'])?>" class="btn btn-success btn-elevate btn-sm">
					<i class="flaticon-search"></i></a></h4>

			<div class="kt-section__content kt-section__content--solid">
				<!-- office name -->
				<h6><?=ucwords($arrexam['office_desc'])?></h6>
				<!-- instruction -->
				<?=ucfirst($arrexam['exam_desc'])?>
			</div>

			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div>
					<a href="<?=base_url('examinee/add_exam_examinee/'.$arrexam['exam_id'])?>" class="btn btn-brand btn-elevate btn-sm">
						<i class="la la-user-plus"></i>
						Add Examinee
					</a>
				</div>
				<div class="row align-items-center" style="padding-top: 15px;">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end: Search Form -->
		</div>
		<div class="kt-portlet__body" style="padding-top: 0px;">
			<!--begin: Datatable -->
			<table class="table-examinee" id="html_table">
				<thead>
					<tr>
						<th>No</th>
						<th>Name</th>
						<th>Email</th>
						<th>Exam Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($arrexaminees as $examinee): $exam_stat = $examinee['finish_exam'];?>
						<tr>
							<td><?=$no++?></td>
							<td><?=fix_fullname($examinee['user_firstname'], $examinee['user_lastname'], $examinee['user_middlename'], $examinee['user_extname'])?></td>
							<td><?=$examinee['user_email']?></td>
							<td><?=$exam_stat?'Finish':'-'?></td>
							<td>
								<a href="javascript:;" class="btn btn-google btn-sm" id="btnremove-examinee" data-id="<?=$examinee['examinee_id']?>">
									<i class="la la-remove"></i>
									Delete
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>

	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="modal-remove-examinee" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-label">Remove Examinee</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<?=form_open('examinee/remove_examinee/'.$arrexam['exam_id'],array('id' => 'frmdelete-set'))?>
				<div class="modal-body">
					<input type="hidden" id="txtexaminee" name="txtexaminee">
					Are you sure you want to remove this examinee?
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-brand">Yes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>
<!--end::Modal-->

<?=load_plugin('js',array('select','form_validation'))?>
<script src="<?=base_url('assets/js/custom/exam-manage_examinee.js')?>"></script>