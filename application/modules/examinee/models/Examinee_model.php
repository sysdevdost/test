<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examinee_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function getdata($examid,$examineeid='')
	{
		if($examineeid!=''):
			$res = $this->db->join('tbluserprofile','tbluserprofile.user_id = tblexaminee.user_id')
							->get_where('tblexaminee',array('exam_id' => $examid, 'exam_id' => $examineeid))->result_array();
			return count($res) > 0 ? $res[0] : array();
		else:
			return $this->db->join('tbluserprofile','tbluserprofile.user_id = tblexaminee.user_id')
							->get_where('tblexaminee',array('exam_id' => $examid, 'is_delete' => 0))->result_array();
		endif;
	}

	public function add($arrdata)
	{
		$this->db->insert('tblexaminee',$arrdata);
		return $this->db->insert_id();
	}

	public function save($arrdata,$examineeid)
	{
		$this->db->where('examinee_id',$examineeid);
		$this->db->update('tblexaminee',$arrdata);
		return $this->db->affected_rows();
	}

	public function check_exist($examineeid,$examid)
	{
		$res = $this->db->get_where('tblexaminee', array('user_id' => $examineeid, 'exam_id' => $examid))->result_array();
		return count($res) > 0 ? $res[0] : array();
	}


}