<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('elipsis'))
{
    function elipsis($string,$chars)
    {
		return strlen($string) > $chars ? substr($string,0,$chars)."..." : $string;
	}
}

if ( ! function_exists('fix_id'))
{
    function fix_id($string)
    {
		return sprintf('%04d', $string);
	}
}

if ( ! function_exists('num_formats'))
{
    function num_formats($format='')
    {
		$formats = array(
					array('name' => 'none', 		 'format' => '(none)',			 		'sample_val' => ''),
					array('name' => 'number', 		 'format' => '1,2,3, ...', 	  	 		'sample_val' => '1'),
					array('name' => 'roman_capital', 'format' => 'I,II,III, ...', 	 		'sample_val' => 'I'),
					array('name' => 'roman_small', 	 'format' => 'i,ii,iii, ...', 	 		'sample_val' => 'i'),
					array('name' => 'letter_capital','format' => 'A,B,C, ...',	  	 		'sample_val' => 'A'),
					array('name' => 'letter_small',  'format' => 'a,b,c, ...', 	  	 		'sample_val' => 'a'),
					array('name' => 'ordinal', 		 'format' => '1st,2nd,3rd, ...', 		'sample_val' => '1st'),
					array('name' => 'number_word', 	 'format' => 'One,Two,Three, ...',		'sample_val' => 'One'),
					array('name' => 'ordinal_word',  'format' => 'First,Second,Third, ...', 'sample_val' => 'First'),
					array('name' => 'sequence_0', 	 'format' => '01,02,03, ...', 			'sample_val' => '01'),
					array('name' => 'sequence_00', 	 'format' => '001,002,003, ...', 		'sample_val' => '001'),
					array('name' => 'sequence_000',  'format' => '0001,0002,0003, ...',		'sample_val' => '0001'),
					array('name' => 'sequence_0000', 'format' => '00001,00002,00003, ...', 	'sample_val' => '00001')
		);

		if($format==''):
			return $formats;
		else:
			return $formats[$format];
		endif;
	}
}

if ( ! function_exists('printa'))
{
    function printa($array)
    {
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}