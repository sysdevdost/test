<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('load_plugin'))
{
    function load_plugin($type,$arrData)
    {
		$CI =& get_instance();
		$str='';
		if($type=="css")
		{
			foreach($arrData as $row):
				switch($row){
					case 'select':
                        $str.= '<link href="'.base_url('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css').'" rel="stylesheet" type="text/css" />'.
							   '<link href="'.base_url('assets/css/demo7/style.bundle.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'select2':
                        $str.= '<link href="'.base_url('assets/vendors/general/select2/dist/css/select2.css').'" rel="stylesheet" type="text/css" />'.
							   '<link href="'.base_url('assets/css/demo7/style.bundle.css').'" rel="stylesheet" type="text/css" />';
                        break;
                    case 'wizard':
                        $str.= '<link href="'.base_url('assets/css/demo7/pages/general/wizard/wizard-2.css').'" rel="stylesheet" type="text/css" />'.
							   '<link href="'.base_url('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css').'" rel="stylesheet" type="text/css" />'.
							   '<link href="'.base_url('assets/css/demo7/style.bundle.css').'" rel="stylesheet" type="text/css" />';
                        break;



					// case 'datatables': $str.='
					// 		<link href="'.base_url('assets/plugins/datatables/datatables.min.css').'" rel="stylesheet" type="text/css" />';
					// 	break;

                    // case 'select':
                    //     $str.=  '<link href="'.base_url('/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css').'" rel="stylesheet" type="text/css" />';
                    //     break;
				}
			endforeach;
			echo $str;
		}
		if($type=="js")
		{
			foreach($arrData as $row):
				switch($row){
					case 'select':
						$str.= '<script src="'.base_url('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js').'" type="text/javascript"></script>';
						break;
					case 'select2':
						$str.= '<script src="'.base_url('assets/vendors/general/select2/dist/js/select2.full.js').'" type="text/javascript"></script>';
						break;
					// case 'datatables':
					// 	$str.= '<script src="'.base_url('assets/vendors/general/block-ui/jquery.blockUI.js').'" type="text/javascript"></script>'.
					// 		   '<script src="'.base_url('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js').'" type="text/javascript"></script>';
					// 	break;


					// case 'datatables': $str.='
					// 		<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'" type="text/javascript"></script>';
					// 	break;
					case 'form_validation': $str.='
							<script src="'.base_url('assets/js/custom/form-validation.js').'" type="text/javascript"></script>
							';
						break;
					case 'form_validation_login': $str.='
							<script src="'.base_url('assets/js/custom/form-validation-login.js').'" type="text/javascript"></script>
							';
						break;
					case 'jquery':
                    	$str.=  '<script src="'.base_url('assets/vendors/general/jquery/dist/jquery.js').'" type="text/javascript"></script>';
                    	break;
         //           	case 'select':
         //           	    	$str.=  '<script src="'.base_url('assets/js/demo7/pages/crud/forms/widgets/bootstrap-select.js').'" type="text/javascript"></script>'.
         //           	    			'<script src="'.base_url('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js').'" type="text/javascript"></script>'.
         //           	    			'<script src="'.base_url('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js').'" type="text/javascript"></script>'.
									// '<script src="'.base_url('assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js').'" type="text/javascript"></script>'.
									// '<script src="'.base_url('assets/js/demo7/pages/crud/forms/widgets/bootstrap-select.js').'" type="text/javascript"></script>';
         //           	    break;
				}
			endforeach;
			echo $str;
		}
	}
}