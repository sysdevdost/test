<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('fix_fullname'))
{
    function fix_fullname($fname, $lname, $mname='', $ext='', $straight=0)
    {
        $lname = $lname!='' ? $lname : '';
    	$mname = $mname == '' ? '' : $mname[0];
    	$mid_ini = str_replace('.', '', $mname);
    	$mid_ini = $mid_ini != '' ? strpos($mid_ini, '.') ? $mid_ini : $mid_ini.'.' : '';
    	$ext = $ext!='' ? $ext.' ': '';
    	$fullname = ucwords($ext.$fname.' '.$mid_ini.' '.$lname);
    	$fullname_rev = ucwords($lname.', '.$fname.' '.$mid_ini);
        return $straight == 0 ? $fullname : $fullname_rev;
	}
}

if ( ! function_exists('access_level'))
{
    function access_level($user_level='')
    {
        $CI =& get_instance();
        $access_level = $CI->db->get('tblaccesslevel')->result_array();

        $session_data = $_SESSION;
		$user_level = $user_level=='' ? $session_data['sess_acc_lvl'] : $user_level;

		switch ($user_level) {
			case 1:
				return 'system administrator'; break;
			case 2:
				return 'examiner'; break;
			case 3:
				return 'examinee'; break;
		}
	}
}

