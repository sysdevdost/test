<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_initial_migrate extends CI_Migration {

        public function up()
        {
                /*Access Level*/
                $this->dbforge->add_field(array(
                        'access_level_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'access_level_descripion' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('access_level_id', TRUE);
                $this->dbforge->create_table('tblaccesslevel');

                /*Answer Sheet*/
                $this->dbforge->add_field(array(
                        'as_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'exe_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'exam_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'quest_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'qitem_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'as_answer' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'null' => TRUE,
                        ),
                        'as_created_date' => array(
                                'type' => 'datetime',
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('as_id', TRUE);
                $this->dbforge->create_table('tblanswersheet');

                /*Examinees Profile*/
                $this->dbforge->add_field(array(
                        'exe_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'exe_firstname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50,
                                'null' => FALSE,
                        ),
                        'exe_middlename' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50,
                                'null' => TRUE,
                        ),
                        'exe_lastname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50,
                                'null' => FALSE,
                        ),
                        'exe_name_ext' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 10,
                                'null' => TRUE,
                        ),
                        'exe_age' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'null' => FALSE,
                        ),
                        'exe_gender' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 1,
                                'null' => TRUE,
                        ),
                        'exe_email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 100,
                                'null' => TRUE,
                        ),
                        'exe_mobilenumber' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 20,
                                'null' => TRUE,
                        ),
                        'exe_created_date' => array(
                                'type' => 'datetime',
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('exe_id', TRUE);
                $this->dbforge->create_table('tblexamineesprofile');

                /*Exams*/
                $this->dbforge->add_field(array(
                        'exam_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'exam_title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 200,
                                'null' => TRUE,
                        ),
                        'exam_description' => array(
                                'type' => 'TEXT',
                                'null' => TRUE,
                        ),
                        'quest_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'random_question_items' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'null' => TRUE,
                        ),
                        'allowed_to_change_answer' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'default' => 0,
                        ),
                        'allowed_to_skip' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'default' => 0,
                        ),
                        'passing_percentage' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'null' => TRUE,
                        ),
                        'is_finished' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'default' => 0,
                        ),
                        'exam_created_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                                'null' => FALSE,
                        ),
                        'exam_created_date' => array(
                                'type' => 'DATETIME'
                        ),
                        'exam_date_from' => array(
                                'type' => 'DATE'
                        ),
                        'exam_date_to' => array(
                                'type' => 'DATE'
                        )
                ));
                $this->dbforge->add_key('exam_id', TRUE);
                $this->dbforge->create_table('tblexams');

                /*Question Item*/
                $this->dbforge->add_field(array(
                        'qitem_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'quest_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'exam_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'qitem_question' => array(
                                'type' => 'TEXT',
                                'null' => TRUE,
                        ),
                        'qitem_option' => array(
                                'type' => 'TEXT',
                                'null' => TRUE,
                        ),
                        'qitem_correct' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'default' => 0,
                        ),
                        'qitem_created_by' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'qitem_created_date' => array(
                                'type' => 'datetime',
                                'null' => FALSE,
                        ),
                        'qitem_modified_by' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'qitem_modified_date' => array(
                                'type' => 'datetime',
                                'null' => TRUE,
                        )
                ));
                $this->dbforge->add_key('qitem_id', TRUE);
                $this->dbforge->create_table('tblquestionitem');

                /*Question Set*/
                $this->dbforge->add_field(array(
                        'quest_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'quest_type_id' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                        ),
                        'quest_instruction' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 300,
                                'null' => TRUE,
                        ),
                        'no_of_option' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'null' => TRUE,
                        ),
                        'time_limit' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'null' => TRUE,
                        ),
                        'quest_created_by' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'quest_created_date' => array(
                                'type' => 'datetime',
                                'null' => FALSE,
                        ),
                        'quest_modified_by' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => TRUE,
                        ),
                        'quest_modified_date' => array(
                                'type' => 'datetime',
                                'null' => TRUE,
                        )
                ));
                $this->dbforge->add_key('quest_id', TRUE);
                $this->dbforge->create_table('tblquestionset');

                /*Question Type*/
                $this->dbforge->add_field(array(
                        'status_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'quest_type_description' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('status_id', TRUE);
                $this->dbforge->create_table('tblquestiontype');

                /*Results*/
                $this->dbforge->add_field(array(
                        'result_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'as_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'score_per_question_set' => array(
                                'type' => 'DECIMAL',
                                'constraint' => '10,2',
                                'null' => FALSE,
                        ),
                        'total_rating' => array(
                                'type' => 'DECIMAL',
                                'constraint' => '10,2',
                                'null' => FALSE,
                        ),
                        'remarks' => array(
                                'type' => 'INT',
                                'constraint' => 2,
                                'null' => FALSE,
                        ),
                        'result_generated_by' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                                'null' => FALSE,
                        ),
                        'result_generated_date' => array(
                                'type' => 'datetime',
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('result_id', TRUE);
                $this->dbforge->create_table('tblresults');

                /*Status*/
                $this->dbforge->add_field(array(
                        'status_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'status_descripion' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 30,
                                'null' => FALSE,
                        ),
                ));
                $this->dbforge->add_key('status_id', TRUE);
                $this->dbforge->create_table('tblstatus');

                /*Users*/
                $this->dbforge->add_field(array(
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'exe_id' => array(
                                'type' => 'INT',
                                'constraint' => 11
                        ),
                        'user_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 100,
                                'null' => FALSE
                        ),
                        'user_password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 150,
                                'null' => FALSE
                        ),
                        'access_level_id' => array(
                                'type' => 'INT',
                                'constraint' => 1,
                                'null' => FALSE
                        ),
                        'office_id' => array(
                                'type' => 'INT',
                                'null' => FALSE,
                                'constraint' => 11
                        ),
                        'status_id' => array(
                                'type' => 'INT',
                                'null' => FALSE,
                                'constraint' => 11
                        ),
                        'hrmis_empnumber' => array(
                                'type' => 'VARCHAR',
                                'null' => TRUE,
                                'constraint' => 20
                        )
                ));
                $this->dbforge->add_key('user_id', TRUE);
                $this->dbforge->create_table('tblusers');
        }

        public function down()
        {
                $this->dbforge->drop_table('tblaccesslevel');
                $this->dbforge->drop_table('tblanswersheet');
                $this->dbforge->drop_table('tblexamineesprofile');
                $this->dbforge->drop_table('tblexams');
                $this->dbforge->drop_table('tblquestionitem');
                $this->dbforge->drop_table('tblquestionset');
                $this->dbforge->drop_table('tblquestiontype');
                $this->dbforge->drop_table('tblresults');
                $this->dbforge->drop_table('tblstatus');
                $this->dbforge->drop_table('tblusers');
        }
}