<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../../../../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
		<title>TEST | Login Page</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Custom Styles(used by this page) -->
		<link href="<?=base_url('assets/css/demo7/pages/general/login/login-3.css')?>" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?=base_url('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?=base_url('assets/vendors/general/tether/dist/css/tether.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/select2/dist/css/select2.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/nouislider/distribute/nouislider.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/dropzone/dist/dropzone.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/summernote/dist/summernote.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/animate.css/animate.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/toastr/build/toastr.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/morris.js/morris.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/sweetalert2/dist/sweetalert2.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/socicon/css/socicon.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/custom/vendors/flaticon/flaticon.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/custom/vendors/flaticon2/flaticon.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?=base_url('assets/css/demo7/style.bundle.css')?>" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="<?=base_url('assets/css/demo7/skins/header/base/light.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/css/demo7/skins/header/menu/light.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/css/demo7/skins/brand/dark.css')?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/css/demo7/skins/aside/dark.css')?>" rel="stylesheet" type="text/css" />

		<link href="<?=base_url('assets/css/custom-login.css')?>" rel="stylesheet" type="text/css">
		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="<?=base_url('assets/media/logos/favicon.ico')?>" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

		<?=$contents?>

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?=base_url('assets/vendors/general/jquery/dist/jquery.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/popper.js/dist/umd/popper.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/js-cookie/src/js.cookie.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/moment/min/moment.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/sticky-js/dist/sticky.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/wnumb/wNumb.js')?>" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->
		<script src="<?=base_url('assets/vendors/general/jquery-form/dist/jquery.form.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/js/demo7/pages/crud/forms/widgets/bootstrap-select.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js')?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/vendors/general/toastr/build/toastr.min.js')?>" type="text/javascript"></script>

		<!-- login script -->
		<script src="<?=base_url('assets/js/custom/login-register.js')?>" type="text/javascript"></script>

		<script>
		    $(document).ready(function(){
		        /* Set flash message */
		        <?php if($this->session->flashdata('strMsg')!=''):?>
		            toastr.warning('<?=$this->session->flashdata('strMsg')?>')
		        <?php endif;?>

		        <?php if($this->session->flashdata('strSuccessMsg')!=''):?>
		            toastr.success('<?=$this->session->flashdata('strSuccessMsg')?>', 'Success')
		        <?php endif;?>

		        <?php if($this->session->flashdata('strErrorMsg')!=''):?>
		            toastr.error('<?=$this->session->flashdata('strErrorMsg')?>')
		        <?php endif;?>
		    });  
		</script>
	</body>

	<!-- end::Body -->
</html>