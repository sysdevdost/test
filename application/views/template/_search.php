<form method="get" class="kt-quick-search__form">
	<div class="input-group">
		<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
		<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
		<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
	</div>
</form>