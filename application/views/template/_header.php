<?php 
	$controller = $this->uri->segment(1);
	$method = $this->uri->segment(2);
	$action = $this->uri->segment(3);
 ?>
 
<!-- <pre>
	<br>Controller: <?=$controller?>
	<br>method: <?=$method?>
	<br>action: <?=$action?>
</pre> -->
<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
	<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout- ">
		<ul class="kt-menu__nav ">
			<li class="kt-menu__item <?=$controller=='dashboard'?'kt-menu__item--active':''?>">
				<a href="<?=base_url('dashboard')?>" class="kt-menu__link ">
					<span class="kt-menu__link-text">Tests</span>
				</a>
			</li>
			<li class="kt-menu__item <?=$controller=='question'?'kt-menu__item--active':''?>">
				<a href="<?=base_url('dashboard')?>" class="kt-menu__link ">
					<span class="kt-menu__link-text">Question Bank</span>
				</a>
			</li>
			<li class="kt-menu__item <?=$controller=='candidates'?'kt-menu__item--active':''?>">
				<a href="<?=base_url('dashboard')?>" class="kt-menu__link ">
					<span class="kt-menu__link-text">Candidates</span>
				</a>
			</li>
		</ul>
	</div>
</div>